package criteriosAceptacionIteracion1;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import json.CrearMocksJson;
import negocio.VoterDistancias;
import notificador.SelectorDeNotificaciones;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;

public class UserStory4 {
	
	static Persona A;
	static Persona B;
	
	static RestriccionPerimetral R1;
	static RestriccionPerimetral R2;
	static SelectorDeNotificaciones n = new SelectorDeNotificaciones();
	
	@Before
	public void inicioEscenario() {
		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
		
		R1 = new RestriccionPerimetral(null, 100, 1, null, A, null);
		R2 = new RestriccionPerimetral(null, 500, 1, null, B, null);
		
		CrearMocksJson.userStory5Criterio3();
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
		Assert.assertEquals(A.getPuntajePeligro().getPuntaje(), 8);
		Assert.assertEquals(B.getPuntajePeligro().getPuntaje(), 4);	}
	
	@Test
	public void criterio1() {
		
		Infraccion infraccion = new Infraccion(0, null, 450, R2);
		Assert.assertEquals(n.notificar(infraccion, R2), 1);
	}

	@Test
	public void criterio2() {
		Infraccion infraccion = new Infraccion(0, null, 350, R2);
		Assert.assertEquals(n.notificar(infraccion, R2), 2);
	}

	@Test
	public void criterio3() {
		Infraccion infraccion = new Infraccion(0, null, 200, R2);
		Assert.assertEquals(n.notificar(infraccion, R2), 3);
	}
	
	@Test
	public void criterio4() {
		Infraccion infraccion = new Infraccion(0, null, 100, R1);
		Assert.assertEquals(n.notificar(infraccion, R1), 2);
	}
	
	@Test
	public void criterio5() {
		Infraccion infraccion = new Infraccion(0, null, 85, R1);
		Assert.assertEquals(n.notificar(infraccion, R1), 3);
	}

	@Test
	public void criterio6() {
		Infraccion infraccion = new Infraccion(0, null, 55, R1);
		Assert.assertEquals(n.notificar(infraccion, R1), 3);
	}
}
