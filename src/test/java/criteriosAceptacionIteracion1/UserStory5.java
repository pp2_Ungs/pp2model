package criteriosAceptacionIteracion1;

import org.junit.BeforeClass;
import org.junit.Test;

import calculadorPuntajePeligro.CalculadorPuntajePeligro;
import json.CrearMocksJson;

import org.junit.Assert;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;

public class UserStory5 {
	
	static Persona A;
	static Persona B;
	
	static RestriccionPerimetral R1;
	static RestriccionPerimetral R2;
	
	@BeforeClass
	public static void inicializarEscenario() {
		CalculadorPuntajePeligro c = new CalculadorPuntajePeligro();
		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
		
		R1 = new RestriccionPerimetral(null, 100, 1, null, A, null);
		R2 = new RestriccionPerimetral(null, 500, 1, null, B, null);
		
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
	}
	
	@Test
	public void criterio1() {
		CrearMocksJson.userStory5Criterio1();
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
		Assert.assertEquals(A.getPuntajePeligro().getPuntaje(), 0);
		Assert.assertEquals(B.getPuntajePeligro().getPuntaje(), 3);
	}
	
	@Test
	public void criterio2() {
		CrearMocksJson.userStory5Criterio2();
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
		Assert.assertEquals(A.getPuntajePeligro().getPuntaje(), 10);
		Assert.assertEquals(B.getPuntajePeligro().getPuntaje(), 5);
	}

	@Test
	public void criterio3() {
		CrearMocksJson.userStory5Criterio3();
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
		Assert.assertEquals(A.getPuntajePeligro().getPuntaje(), 8);
		Assert.assertEquals(B.getPuntajePeligro().getPuntaje(), 4);
	}
	
}
