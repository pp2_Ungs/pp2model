package criteriosAceptacionIteracion1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import analizadorInfracciones.AnalizadorInfracciones;
import json.CrearMocksJson;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;
import pojos.TipoInfraccion;

public class UserStory1 {
	static Persona A;
	static Persona B;
	
	static RestriccionPerimetral R1;
	static RestriccionPerimetral R2;

	@BeforeClass
	public static void inicioEscenario() {
		AnalizadorInfracciones analizador = new AnalizadorInfracciones();
		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
		
		R1 = new RestriccionPerimetral(null, 100, 1, null, A, null);
		R2 = new RestriccionPerimetral(null, 500, 1, null, B, null);
		CrearMocksJson.userStory1IT2Criterio1();
	}
	
	@Test
	public void criterio1() {
		Infraccion primeraInfraccion = new Infraccion(1, null, 100, R1);
		Assert.assertEquals(TipoInfraccion.LEVE, primeraInfraccion.getTipoInfraccion());
	}

	@Test
	public void criterio2() {
		Infraccion primeraInfraccion = new Infraccion(1, null, 60, R1);
		Assert.assertEquals(TipoInfraccion.GRAVE, primeraInfraccion.getTipoInfraccion());
	}

	@Test
	public void criterio3() {
		Infraccion primeraInfraccion = new Infraccion(1, null, 350, R2);
		Assert.assertEquals(TipoInfraccion.MODERADA, primeraInfraccion.getTipoInfraccion());
	}
	
	@Test
	public void criterio4() {
		Infraccion primeraInfraccion = new Infraccion(1, null, 150, R2);
		Assert.assertEquals(TipoInfraccion.GRAVE, primeraInfraccion.getTipoInfraccion());
	}
	
	@Test(expected = Exception.class)
	public void criterio5() {
		RestriccionPerimetral r = new RestriccionPerimetral(null, 50, 21, null, null, null);
		Infraccion i = new Infraccion(55, null, 100, r);
	}
}
