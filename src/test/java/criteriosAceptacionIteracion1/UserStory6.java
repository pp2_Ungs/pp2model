package criteriosAceptacionIteracion1;

import org.junit.BeforeClass;
import org.junit.Test;

import calculadorPuntajePeligro.CalculadorPuntajePeligro;
import json.CrearMocksJson;
import observer.ControladorCoordenadas;
import observer.ControladorDistancias;
import observer.UbicacionAgresor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;

import org.junit.Assert;

import pojos.Coordenada;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;

public class UserStory6 {

	static Persona A;
	static Persona B;

	static RestriccionPerimetral R1;
	static RestriccionPerimetral R2;

	static ControladorDistancias consultor = new ControladorDistancias();
	static ControladorCoordenadas ubi = new ControladorCoordenadas();

	static List<Persona> agresores = new ArrayList<Persona>();

	@BeforeClass
	public static void inicializarEscenario() {
		CalculadorPuntajePeligro c = new CalculadorPuntajePeligro();
		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");

		R1 = new RestriccionPerimetral(null, 100, 1, null, A, null);
		R2 = new RestriccionPerimetral(null, 500, 1, null, B, null);

		CrearMocksJson.userStory5Criterio3();
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);

		ubi.enlazarObservador(consultor);
	}

	@Test
	public void criterioAgresorA() throws InterruptedException {
		agresores.add(A);
		ubi.consultarUbicaciones(agresores);
		Assert.assertNull(ubi.listaAgresoresMasPeligrosos.getCoordenadasAgresor().get(3));

		Thread.sleep(1000);
		Coordenada ubicacionInicial = ubi.listaAgresoresMasPeligrosos.getCoordenadasAgresor().get(A);

		Thread.sleep(5000);
		Coordenada ubicacionActual = ubi.listaAgresoresMasPeligrosos.getCoordenadasAgresor().get(A);
		Assert.assertTrue(ubicacionInicial.equals(ubicacionActual));

		Thread.sleep(10000);
		ubicacionActual = ubi.listaAgresoresMasPeligrosos.getCoordenadasAgresor().get(A);
		Assert.assertFalse(ubicacionInicial.equals(ubicacionActual));

	}

	@Test
	public void criterioAgresorB() throws InterruptedException {
		agresores.clear();
		agresores.add(B);

		ubi.consultarUbicaciones(agresores);
		Assert.assertNull(ubi.listaAgresoresMenosPeligrosos.getCoordenadasAgresor().get(B));

		Thread.sleep(1000);
		Coordenada ubicacionInicial = ubi.listaAgresoresMenosPeligrosos.getCoordenadasAgresor().get(B);
		
		Thread.sleep(15000);
		Coordenada ubicacionActual = ubi.listaAgresoresMenosPeligrosos.getCoordenadasAgresor().get(B);
		Assert.assertTrue(ubicacionInicial.equals(ubicacionActual));

		Thread.sleep(30000);
		ubicacionActual = ubi.listaAgresoresMenosPeligrosos.getCoordenadasAgresor().get(B);
		Assert.assertFalse(ubicacionInicial.equals(ubicacionActual));
		
		ubi.desenlazarObservador(consultor);
	}

}
