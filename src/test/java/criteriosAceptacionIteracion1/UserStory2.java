package criteriosAceptacionIteracion1;

import java.util.List;

import org.junit.Test;

import distanceGateway.DistanceGateway;
import distanceGateway.DistanceGatewayStub;

import org.junit.Assert;
import pojos.Coordenada;

public class UserStory2 {

	DistanceGateway gate = new DistanceGatewayStub();
	Coordenada coord1 = new Coordenada(-2, 5);
	Coordenada coord2 = new Coordenada(1, 3);

	@Test
	public void criterioA() {
		List<Integer> distancias = gate.getDistancias(coord1, coord2);
		Assert.assertEquals(distancias.get(0).intValue(), 1500);
		Assert.assertEquals(distancias.get(1).intValue(), 1510);
		Assert.assertEquals(distancias.get(2).intValue(), 1505);
	}

	@Test
	public void criterioB() {
		((DistanceGatewayStub) gate).setFailS3();
		List<Integer> distancias = gate.getDistancias(coord1, coord2);
		Assert.assertEquals(distancias.get(0).intValue(), 1500);
		Assert.assertEquals(distancias.get(1).intValue(), 1510);
		Assert.assertEquals(distancias.get(2), null);
		
	}
	
}
