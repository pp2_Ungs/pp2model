package criteriosAceptacionIteracion1;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import negocio.AnalizadorDistancias;
import negocio.VoterDistancias;

public class UserStory3 {

	List<Integer> listaDistancias = new ArrayList<Integer>();

	@Before
	public void inicioEscenario() {
		listaDistancias.clear();
	}

	@Test
	public void criterioA() {

		listaDistancias.add(1500);
		listaDistancias.add(1510);
		listaDistancias.add(1500);
		Integer distanciaVotada;
		distanciaVotada = VoterDistancias.elegirDistancia(listaDistancias);

		Assert.assertEquals(1500, distanciaVotada.intValue());
	}

	@Test
	public void criterioB() {
		listaDistancias.add(340);
		listaDistancias.add(345);
		listaDistancias.add(null);
		Integer distanciaVotada;

		distanciaVotada = VoterDistancias.elegirDistancia(listaDistancias);

		Assert.assertEquals(340, distanciaVotada.intValue());
	}

	@Test
	public void criterioC() {
		listaDistancias.add(null);
		listaDistancias.add(null);
		listaDistancias.add(null);
		Integer distanciaVotada;

		distanciaVotada = VoterDistancias.elegirDistancia(listaDistancias);

		Assert.assertEquals(null, distanciaVotada);

	}

	@Test(expected = Exception.class)
	public void criterioD() throws Exception {
		listaDistancias.add(100);
		listaDistancias.add(110);
		listaDistancias.add(200);
		Integer distanciaVotada;

		distanciaVotada = VoterDistancias.elegirDistancia(listaDistancias);

		Assert.assertEquals(100, distanciaVotada.intValue());
		
		AnalizadorDistancias.compararDistancias(listaDistancias);

	}
}
