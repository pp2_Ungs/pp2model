package codeCoverage;

import org.junit.Test;

import distanceGateway.DistanceGateway;
import distanceGateway.DistanceGatewayStub;
import pojos.Coordenada;

import java.util.List;

import org.junit.Assert;

public class DistanceGatewayTest {

	
	@Test
	public void testGateway() {
		DistanceGateway gate = new DistanceGateway();
		List<Integer> distancias = gate.getDistancias(new Coordenada (1,1), new Coordenada (4,5));

		Assert.assertEquals(distancias.get(0).intValue(), 5);
		Assert.assertEquals(distancias.get(1).intValue(), 15);
		Assert.assertEquals(distancias.get(2).intValue(), 10);
	}
	
	@Test
	public void testGatewayStub() {
		DistanceGatewayStub gate = new DistanceGatewayStub();
		gate.setFailAll();
		List<Integer> distancias = gate.getDistancias(new Coordenada (1,1), new Coordenada (4,5));
		
		for(Integer i : distancias)
			Assert.assertNull(i);
	}

}
