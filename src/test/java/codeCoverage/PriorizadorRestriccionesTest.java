package codeCoverage;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import json.JsonReaders;
import org.junit.Assert;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;
import priorizadorRestricciones.ComparadorDeRestricciones;
import priorizadorRestricciones.Criterio;
import priorizadorRestricciones.CriterioPuntajeAgresor;
import priorizadorRestricciones.PriorizadorDeRestricciones;
import priorizadorRestricciones.Puntaje;

public class PriorizadorRestriccionesTest {
	
	static Persona A;
	static Persona B;
	
	static RestriccionPerimetral R1;
	static RestriccionPerimetral R2;
	
	static Criterio c;

	@BeforeClass
	public static void inicializarEscenario() {
	
		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
		
		R1 = new RestriccionPerimetral(null, 100, 1, null, A, null);
		R2 = new RestriccionPerimetral(null, 100, 2, null, B, null);

		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
		c = new CriterioPuntajeAgresor();
		
		Puntaje p = new Puntaje();
		
	}
	
	@Test
	public void testPriorizador() {
		PriorizadorDeRestricciones priorizador = new PriorizadorDeRestricciones();
		List<RestriccionPerimetral> restriccionesPriorizadas = priorizador.priorizar(JsonReaders.readRestriccionesPerimetrales(), c);
		List<RestriccionPerimetral> expected = new ArrayList<RestriccionPerimetral>();
		
		expected.add(R1);
		expected.add(R2);
		

		for(int i = 0; i<expected.size();i++) {
			Assert.assertEquals(expected.get(i).getId(), restriccionesPriorizadas.get(i).getId());
		}
		ComparadorDeRestricciones cr = new ComparadorDeRestricciones(JsonReaders.readRestriccionesPerimetrales(), c);
		cr.getPuntajes();
	}

}
