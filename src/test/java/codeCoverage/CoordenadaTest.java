package codeCoverage;

import org.junit.Test;
import org.junit.Assert;
import pojos.Coordenada;

public class CoordenadaTest {
	
	Coordenada c1 = new Coordenada(-5, 10);
	Coordenada c2 = new Coordenada(40, 45);
	
	@Test
	public void testCoordenada1() {
		
		
		Assert.assertEquals(-5, c1.getX());
		Assert.assertEquals(45, c2.getY());
		Assert.assertNotEquals(-10, c2.getX());
	}
	
	@Test
	public void testCoordenadaSetters() {
		c1.setX(1);
		c1.setY(4);
		
		Assert.assertEquals(1, c1.getX());
		Assert.assertEquals(4, c1.getY());
	}
	
	@Test
	public void testCoordenadaToString() {
		String expected = "( -5 , 10 )";
		Assert.assertEquals(c1.toString() ,expected);
	}

}
