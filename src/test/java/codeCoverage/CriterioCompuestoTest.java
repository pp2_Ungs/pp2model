package codeCoverage;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import json.CrearMocksJson;
import pojos.Coordenada;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;
import priorizadorRestricciones.Criterio;
import priorizadorRestricciones.CriterioCompuesto;
import priorizadorRestricciones.CriterioInfraccionesGraves;
import priorizadorRestricciones.CriterioInfraccionesLeves;
import priorizadorRestricciones.CriterioInfraccionesModerada;

public class CriterioCompuestoTest {

	static Persona A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
	static Persona B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
	static Persona C = new Persona("C", "C", "987654321", RolEnRestriccion.AGRESOR, 9, "", "");
	
	static RestriccionPerimetral R1 = new RestriccionPerimetral(null, 100, 1, null, A, new Coordenada(0, 0));
	static RestriccionPerimetral R2 = new RestriccionPerimetral(null, 500, 2, null, B, new Coordenada(0, 0));
	static RestriccionPerimetral R3 = new RestriccionPerimetral(null, 100, 3, null, C, new Coordenada(0, 0));
	
	static Criterio criterioPeligrosidad;
	static Criterio criterioInfraccionesLeves;
	static Criterio criterioInfraccionesModeradas;
	static Criterio criterioInfraccionesGraves;
	static Criterio compuesto;

	@BeforeClass
	public static void inicializarEscenario() {

		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
		C.getPuntajePeligro().actualizarPuntaje(9);
		
		criterioInfraccionesLeves = new CriterioInfraccionesLeves();
		criterioInfraccionesModeradas = new CriterioInfraccionesModerada();
		criterioInfraccionesGraves = new CriterioInfraccionesGraves();
		
		Collection<Criterio> lista = new ArrayList<Criterio>();
		lista.add(criterioInfraccionesLeves);
		lista.add(criterioInfraccionesModeradas);
		lista.add(criterioInfraccionesGraves);
		

		compuesto = new CriterioCompuesto(lista);
		CrearMocksJson.muchasInfracciones();
		
	}
	
	@Test
	public void criterio1(){
		compuesto.puntuar(R2);
		compuesto.puntuar(R3);
		
		System.out.println("PUNTAJE R1 "+		compuesto.puntuar(R1)+" Puntaje persona A "+A.getPuntajePeligro().getPuntaje());
		System.out.println("PUNTAJE R2 "+		compuesto.puntuar(R2)+" Puntaje persona B "+B.getPuntajePeligro().getPuntaje());
		System.out.println("PUNTAJE R3 "+		compuesto.puntuar(R3)+" Puntaje persona C "+C.getPuntajePeligro().getPuntaje());
		
	
	}


}
