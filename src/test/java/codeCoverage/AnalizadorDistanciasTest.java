package codeCoverage;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

import negocio.AnalizadorDistancias;

public class AnalizadorDistanciasTest {

	AnalizadorDistancias a = new AnalizadorDistancias();
	List<Integer> distancias = new ArrayList<Integer>();

	@Test
	public void testSinDistanciasGrandes() {
		try {
			distancias.add(400);
			distancias.add(401);
			AnalizadorDistancias.compararDistancias(distancias);
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.assertTrue(false);
		}
	}

	@Test(expected = Exception.class)
	public void testDistanciasGrandes() throws Exception {
		distancias.add(400);
		distancias.add(4001);
		AnalizadorDistancias.compararDistancias(distancias);
		Assert.assertTrue(false);

		Assert.assertTrue(true);

	}
}
