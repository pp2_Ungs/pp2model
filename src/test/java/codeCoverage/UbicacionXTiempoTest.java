package codeCoverage;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import pojos.Coordenada;
import pojos.Persona;
import pojos.RolEnRestriccion;
import pojos.UbicacionDiaHora;

public class UbicacionXTiempoTest {
	
	static Persona A;
	static UbicacionDiaHora ubicacion;
	static Coordenada coordenada = new Coordenada(110, 10);
	
	@BeforeClass
	public static void inicioEscenario() {
		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		ubicacion = new UbicacionDiaHora(0, A, coordenada, 1, 12);		
	}

	@Test
	public void criterio1() {
		Assert.assertEquals(110, ubicacion.getUbicacion().getX());
		Assert.assertEquals(10, ubicacion.getUbicacion().getY());		
	}

	@Test
	public void criterio2() {
		Assert.assertEquals(1, ubicacion.getDiaDeSemana());	
	}

	@Test
	public void criterio4() {
		UbicacionDiaHora ubi = new UbicacionDiaHora(1, A, coordenada, 2, 14); 
		Assert.assertEquals(1, ubi.getId());
	}

}
