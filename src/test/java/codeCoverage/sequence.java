package codeCoverage;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

import json.CrearMocksJson;
import json.JsonReaders;
import negocio.AnalizadorDistancias;
import observer.ControladorCoordenadas;
import observer.ControladorDistancias;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;

public class sequence {

	public static void main(String[] args) {

		Persona A = new Persona("Alberto", "Gomez", "123456", RolEnRestriccion.AGRESOR, 8, "", "");
		RestriccionPerimetral R = JsonReaders.readRestriccionesPerimetrales().get(1);
		CrearMocksJson.muchasInfracciones();

		A.getPuntajePeligro().actualizarPuntaje(3);

		List<Persona> agresores = new ArrayList<Persona>();
		agresores.add(A);

		System.out.println("Tenemos la persona Alberto Gomez, identificado con el ID=8, que es un agresor con un puntaje de peligro de "
				+ A.getPuntajePeligro().getPuntaje());
		System.out.println("Alberto tiene una restricción perimetral a la ubicación " + R.getUbicacion()
				+ " establecida con una distancia de " + R.getDistancia()+" metros.");
		System.out.println(
				"Dado que el agresor tiene un puntaje de peligro de 9 puntos, se consultará su ubicación cada 10 segundos para verificar que no este realizando una violacion a la restriccion perimetral");
		System.out.println("A continuación realizaremos una simulación de su seguimiento en tiempo real para ir detectando si realiza infracciones");
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

		ControladorDistancias consultor = new ControladorDistancias();
		ControladorCoordenadas ubi = new ControladorCoordenadas();
		ubi.enlazarObservador(consultor);
		ubi.consultarUbicaciones(agresores);

	}

}
