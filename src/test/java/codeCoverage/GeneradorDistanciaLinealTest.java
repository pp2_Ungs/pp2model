package codeCoverage;

import org.junit.Assert;
import org.junit.Test;

import negocio.CalculadorDistancias;
import pojos.Coordenada;

public class GeneradorDistanciaLinealTest {

	@Test
	public void testDistanciaLineal() {
		Coordenada x = new Coordenada(0, 0);
		Coordenada y = new Coordenada(3, 0);		
		CalculadorDistancias generador = new CalculadorDistancias();	
		Assert.assertEquals(generador.generarDistanciaLineal(x, y), 3);
	}
}
