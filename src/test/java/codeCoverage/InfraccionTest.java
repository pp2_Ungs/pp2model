package codeCoverage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Assert;
import org.junit.Test;

import negocio.ValidadorInfraccion;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;
import pojos.TipoInfraccion;

public class InfraccionTest {

	@Test
	public void testGetters() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		Date fechaActual = null;
		try {
			fechaActual = dateFormat.parse("28/03/2018 15:15");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Persona A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		Persona B = new Persona("B", "B", "1456", RolEnRestriccion.VICTIMA, 7, "", "");
		
		RestriccionPerimetral restriccion = new RestriccionPerimetral(fechaActual, 100, 1, B, A, null);
		Infraccion primeraInfraccion = new Infraccion(1, fechaActual, 92, restriccion);
		TipoInfraccion.valueOf("LEVE");

		Assert.assertEquals(restriccion.getVictima().getIdPersona(), 7);
		Assert.assertEquals(92.0, primeraInfraccion.getDistancia(), 4);
		Assert.assertEquals(fechaActual, primeraInfraccion.getFecha());
		Assert.assertEquals(1, primeraInfraccion.getId());
		Assert.assertEquals(1, primeraInfraccion.getRestriccionPerimetral().getId());
		Assert.assertEquals(
				"INFRACCION:\n              id = 1,   fecha = 28/03/2018 03:15,   distancia = 92,   tipo = LEVE,   id Restriccion Perimetral = 1",
				primeraInfraccion.toString());
		Assert.assertEquals(fechaActual, restriccion.getFechaSentencia());
		Assert.assertEquals(
				"RESTRICCIONES PERIMETRALES:\n                             id = 1,   distancia = 100",
				restriccion.toString());

	}
	
	@Test(expected = Exception.class)
	public void testInfraccionConNegativos() {
		ValidadorInfraccion validador = new ValidadorInfraccion();
		validador.validarDistanciaInfraccion(40, -50);
		RestriccionPerimetral r = new RestriccionPerimetral(null, -50, 21, null, null, null);
		Infraccion i = new Infraccion(55, null, 100, r);
	}
}
