package codeCoverage;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import json.JsonReaders;
import notificador.SelectorDeNotificaciones;

import org.junit.Assert;

import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;
import priorizadorRestricciones.Criterio;
import priorizadorRestricciones.PriorizadorDeRestricciones;

public class NotificadorTest {

	static Persona A;
	static Persona B;
	static Persona C;
	static Persona D;
	static RestriccionPerimetral R1;
	static RestriccionPerimetral R2;
	static SelectorDeNotificaciones selector;
	Infraccion i;

	@BeforeClass
	public static void inicializarEscenario() {

		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
		C = new Persona("C", "C", "987654321", RolEnRestriccion.VICTIMA, 0, "", "");
		D = new Persona("D", "D", "987654321", RolEnRestriccion.CONTACTOVICTIMA, 0, "a@gmail.com", "1234");

		R1 = new RestriccionPerimetral(null, 100, 1, null, A, null);
		R2 = new RestriccionPerimetral(null, 100, 2, null, B, null);

		selector = new SelectorDeNotificaciones();

		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);

		R1.agregarContactoVictima(D);
	}

	@Test
	public void testNotificacionGrave() {
		i = new Infraccion(99, null, 20, R1);
		Assert.assertEquals(3, selector.notificar(i, R1));

	}

	@Test
	public void testNotificacionModerada() {
		i = new Infraccion(99, null, 74, R2);
		Assert.assertEquals(2, selector.notificar(i, R2));
		
		i = new Infraccion(99, null, 90, R1);
		Assert.assertEquals(2, selector.notificar(i, R1));

	}

	@Test
	public void testNotificacionLeve() {
		i = new Infraccion(99, null, 90, R2);

		Assert.assertEquals(1, selector.notificar(i, R2));

	}
}
