package codeCoverage;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import distanceGateway.DistanceGateway;
import distanceGateway.S1Mock;
import distanceGateway.S2Mock;
import distanceGateway.S3Mock;
import pojos.Coordenada;

public class MockServicesTest {
	
	S1Mock s1 = new S1Mock();
	S2Mock s2 = new S2Mock();
	S3Mock s3 = new S3Mock();
	
	@Test
	public void testErrorS1() {
		Assert.assertEquals(-2, s1.getDistance(4, 5, 6, 7, "imperial").intValue());
	}
	
	@Test
	public void testErrorS2() {
		Assert.assertEquals(-2, s2.getDistance(4, 5, 6, 7, "chinese", "json").intValue());
		Assert.assertEquals(-3, s2.getDistance(4, 5, 6, 7, "spanish", "xml").intValue());
	}
	
	@Test
	public void testErrorS3() {
		Assert.assertEquals(-2, s3.getDistance(4, 5, 6, 7, "driving").intValue());
	}
	
}
