package codeCoverage;

import pojos.Persona;
import pojos.RolEnRestriccion;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class PersonaTest {
	
	Persona A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "1465");

	@Test
	public void testGetters() {
		RolEnRestriccion.valueOf("VICTIMA");
		Assert.assertEquals(A.getApellido(), "A");
		Assert.assertEquals(A.getNombre(), "A");
		Assert.assertEquals(A.getDNI(), "123456");
		Assert.assertEquals(A.getRolPersona(), RolEnRestriccion.AGRESOR);
		Assert.assertEquals(A.getIdPersona(), 3);
		Assert.assertEquals("1465", A.getNumeroTelefono());
		Assert.assertEquals("", A.getEmail());
	}
}
