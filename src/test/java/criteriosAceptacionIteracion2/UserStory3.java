package criteriosAceptacionIteracion2;

import org.junit.Test;

import notificadorGateway.NotificadorGateway;

import org.junit.Assert;
import pojos.Coordenada;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;

public class UserStory3 {
	
	NotificadorGateway n = new NotificadorGateway();
	
	@Test
	public void enviarWhatsapp() {
		Assert.assertTrue(n.notificar("whatsapp", "1150638997", "aa@gmail.com", "Violacion a restriccion perimetral"));
	}
	
	@Test
	public void enviarSms() {
		Assert.assertTrue(n.notificar("sms", "1150638997", "aa@gmail.com", "Violacion a restriccion perimetral"));
	}
	
	@Test
	public void enviarMail() {
		Assert.assertTrue(n.notificar("email", "1150638997", "gfgrillo3@gmail.com", "Violacion a restriccion perimetral"));
	}
	
	@Test
	public void fallarWhatsapp() {
		Assert.assertFalse(n.notificar("whatsapp", "0", "gfgrillo3@gmail.com", "Violacion a restriccion perimetral"));

	}
	
	@Test
	public void fallarMail() {
		Assert.assertFalse(n.notificar("email", "11506384521997", "aa.com", "Violacion a restriccion perimetral"));

	}
	
}