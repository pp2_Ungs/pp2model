package criteriosAceptacionIteracion2;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import pojos.Coordenada;
import pojos.Persona;
import pojos.RolEnRestriccion;
import rutinaAgresor.ControladorRutina;

public class UserStory2 {
	
	static Persona A;
	static ControladorRutina controlador;

	
	@BeforeClass
	public static void inicioEscenario() {

		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		controlador = new ControladorRutina();
		
	}
	
	@Test
	public void criterio1() {
		Coordenada coordenada = new Coordenada(311, 259);
		Assert.assertTrue(controlador.estaEnRutina(A, coordenada, 12, 1));
	}

	@Test
	public void criterio2() {
		Coordenada coordenada = new Coordenada(311, 238);
		Assert.assertFalse(controlador.estaEnRutina(A, coordenada, 12, 1));
	}

	@Test
	public void criterio3() {
		Coordenada coordenada = new Coordenada(290, 259);
		Assert.assertFalse(controlador.estaEnRutina(A, coordenada, 12, 1));
	}

	@Test
	public void criterio4() {
		Coordenada coordenada = new Coordenada(291, 239);
		Assert.assertTrue( controlador.estaEnRutina(A, coordenada, 12, 1));
	}

	@Test
	public void criterio5() {
		Coordenada coordenada = new Coordenada(305, 255);
		Assert.assertTrue(controlador.estaEnRutina(A, coordenada, 12, 1));
	}

	@Test
	public void criterio6() {
		Coordenada coordenada = new Coordenada(200, 200);
		Assert.assertFalse(controlador.estaEnRutina(A, coordenada, 12, 1));
	}

}
