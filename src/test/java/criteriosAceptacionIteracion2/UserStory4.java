package criteriosAceptacionIteracion2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import json.CrearMocksJson;
import json.JsonReaders;
import pojos.Coordenada;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;
import pojos.TipoInfraccion;
import rutinaAgresor.ControladorRutina;
import stateInfraccion.ControladorInfracciones;

public class UserStory4 {
	
	static Persona B;
	static RestriccionPerimetral R2;
	static ControladorInfracciones controlador;
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static Date fecha;
	static List<Infraccion> listaInfracciones;

	
	@BeforeClass
	public static void inicioEscenario() {

		B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
		R2 = new RestriccionPerimetral(null, 500, 2, null, B, null);

		
		CrearMocksJson.userStory1IT2Criterio1();
		B.getPuntajePeligro().actualizarPuntaje(8);
		
		controlador = new ControladorInfracciones();

		
	}
	
	@Test
	public void criterio1() {
		setFecha("2019-05-30 04:35:00");
		controlador.verificarInfraccion(R2, 400, fecha);
		
		Infraccion infraccion = ultimaInfraccion();
		Infraccion expected = new Infraccion(0, fecha, 400, R2);
		
		Assert.assertTrue(infraccion.equals(expected));
		Assert.assertTrue(infraccion.getTipoInfraccion().equals(TipoInfraccion.LEVE));
	}

	@Test
	public void criterio2() {
		setFecha("2019-05-30 04:36:00");
		controlador.verificarInfraccion(R2, 283, fecha);
		
		Infraccion infraccion = ultimaInfraccion();
		Infraccion expected = new Infraccion(0, fecha, 283, R2);
		
		System.out.println();
		Assert.assertTrue(infraccion.equals(expected));
		Assert.assertTrue(infraccion.getTipoInfraccion().equals(TipoInfraccion.MODERADA));
	}

	@Test
	public void criterio3() {
		setFecha("2019-05-30 04:37:00");
		controlador.verificarInfraccion(R2, 145, fecha);
		
		Infraccion infraccion = ultimaInfraccion();
		Infraccion expected = new Infraccion(0, fecha, 145, R2);
		Assert.assertTrue(infraccion.equals(expected));
		Assert.assertTrue(infraccion.getTipoInfraccion().equals(TipoInfraccion.GRAVE));

	}

	@Test
	public void criterio4() {
		setFecha("2019-05-30 04:38:00");
		controlador.verificarInfraccion(R2, 95, fecha);
		
		Infraccion infraccion = ultimaInfraccion();
		Infraccion expected = new Infraccion(0, fecha, 95, R2);
		Assert.assertTrue(infraccion.equals(expected));
		Assert.assertTrue(infraccion.getTipoInfraccion().equals(TipoInfraccion.GRAVE));

	}

	@Test
	public void criterio5() {
		setFecha("2019-05-30 04:39:00");
		controlador.verificarInfraccion(R2, 350, fecha);
		
		Infraccion infraccion = ultimaInfraccion();
		Infraccion expectedFalse = new Infraccion(0, fecha, 350, R2);
		
		setFecha("2019-05-30 04:38:00");
		Infraccion expectedTrue = new Infraccion(0, fecha, 95, R2);

		Assert.assertFalse(infraccion.equals(expectedFalse));
		Assert.assertTrue(infraccion.equals(expectedTrue));
	}
	
	private void setFecha(String fecha) {
		try {
			this.fecha = sdf.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
	
	private Infraccion ultimaInfraccion() {
		readInfracciones();
		return listaInfracciones.get(listaInfracciones.size()-1);

	}
	
	private void readInfracciones() {
		listaInfracciones = JsonReaders.readInfraccionesPorPersona(B.getIdPersona());
	}
	
	

}