package criteriosAceptacionIteracion2;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import json.CrearMocksJson;
import json.JsonReaders;
import pojos.Coordenada;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;
import pojos.TipoInfraccion;
import priorizadorRestricciones.Criterio;
import priorizadorRestricciones.CriterioAgresorRutinario;
import priorizadorRestricciones.CriterioCompuesto;
import priorizadorRestricciones.CriterioInfraccionesGraves;
import priorizadorRestricciones.CriterioInfraccionesLeves;
import priorizadorRestricciones.CriterioInfraccionesModerada;
import priorizadorRestricciones.CriterioPuntajeAgresor;
import priorizadorRestricciones.PriorizadorDeRestricciones;

public class UserStory1 {
	
	static Persona A;
	static Persona B;
	static Persona C;

	
	static RestriccionPerimetral R1;
	static RestriccionPerimetral R2;
	static RestriccionPerimetral R3;

	static Criterio c;
	static List<Criterio> criterios;
	
	static PriorizadorDeRestricciones priorizador = new PriorizadorDeRestricciones();
	
	static List<RestriccionPerimetral> restricciones = new ArrayList<RestriccionPerimetral>();

	
	@BeforeClass
	public static void inicializarEscenario() {
	
		A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
		B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
		C = new Persona("C", "C", "987654321", RolEnRestriccion.AGRESOR, 9, "", "");

		
		R1 = new RestriccionPerimetral(null, 100, 1, null, A, null);
		R2 = new RestriccionPerimetral(null, 500, 2, null, B, null);
		R3 = new RestriccionPerimetral(null, 100, 3, null, C, null);

		

		CrearMocksJson.userStory1IT2Criterio1();
		B.getPuntajePeligro().actualizarPuntaje(8);
		A.getPuntajePeligro().actualizarPuntaje(3);
		C.getPuntajePeligro().actualizarPuntaje(9);
		
		Criterio criterioInfraccionesGraves = new CriterioInfraccionesGraves();
		Criterio criterioInfraccionesModeradas = new CriterioInfraccionesModerada();
		Criterio criterioInfraccionesLeves = new CriterioInfraccionesLeves();
		Criterio criterioAgresorRutinario = new CriterioAgresorRutinario();
		Criterio criterioPuntajeAgresor = new CriterioPuntajeAgresor();
		
		List<Criterio> criterios = new ArrayList<Criterio>();
		criterios.add(criterioInfraccionesGraves);
		criterios.add(criterioInfraccionesModeradas);
		criterios.add(criterioInfraccionesLeves);
		criterios.add(criterioAgresorRutinario);
		criterios.add(criterioPuntajeAgresor);
		
		c = new CriterioCompuesto(criterios);
		
		restricciones.add(R1);
		restricciones.add(R2);
		restricciones.add(R3);
		
	}

	@Test
	public void CA1(){
		
		Assert.assertEquals(c.puntuar(R1), 3.5, 0);
		Assert.assertEquals(c.puntuar(R2), 0.0, 0);
		
	}
	
	@Test
	public void CA2(){
		Assert.assertEquals(c.puntuar(R3), 2.0, 0);
		
	}
	
	@Test
	public void testPriorizador() {
		
		List<RestriccionPerimetral> restriccionesPriorizadas = priorizador.priorizar(restricciones, c);
		List<RestriccionPerimetral> expected = new ArrayList<RestriccionPerimetral>();
		
		expected.add(R1);
		expected.add(R3);
		expected.add(R2);
				
		Assert.assertEquals(expected.size(), restriccionesPriorizadas.size());

		for(int i = 0; i<expected.size();i++) {
			Assert.assertEquals(expected.get(i).getId(), restriccionesPriorizadas.get(i).getId());
		}
	}
	
	/*
	@Test
	public void criterio1(){
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);	
		c = new CriterioInfracciones();
		Assert.assertEquals(c.puntuar(R1), 20);
		Assert.assertEquals(c.puntuar(R2), 30);
	}
	
	@Test
	public void criterio2(){
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);	
		c = new CriterioPuntajePeligrosidadVictimario();		
		Assert.assertEquals(c.puntuar(R1), 40);
		Assert.assertEquals(c.puntuar(R2), 20);
	}

	@Test
	public void criterio3() {
		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);
		Criterio criterioInfracciones = new CriterioInfracciones();
		Criterio criterioPeligrosidad = new CriterioPuntajePeligrosidadVictimario();		
		List<Criterio> criterios = new ArrayList<Criterio>();
		criterios.add(criterioPeligrosidad);
		criterios.add(criterioInfracciones);
		
		c = new CriterioCompuesto(criterios);
		Assert.assertEquals(c.puntuar(R1), 60);
		Assert.assertEquals(c.puntuar(R2), 50);
	}

*/
}
