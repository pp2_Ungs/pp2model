package priorizadorRestricciones;

import pojos.RestriccionPerimetral;

public class CriterioPuntajeAgresor implements Criterio {

	@Override
	public double puntuar(RestriccionPerimetral restriccion) {
		int puntaje = restriccion.getAgresor().getPuntajePeligro().getPuntaje();

		if (puntaje < 5) {
			System.out.println("PUNTAJE AGRESOR SUMA 0");

			return 0;}
		if (puntaje <= 6) {			System.out.println("PUNTAJE AGRESOR SUMA 0.5");

			return 0.5;}
		if (puntaje <= 8) {			System.out.println("PUNTAJE AGRESOR SUMA 1");

			return 1;}
		System.out.println("PUNTAJE AGRESOR SUMA 2");

		return 2;

	}

}
