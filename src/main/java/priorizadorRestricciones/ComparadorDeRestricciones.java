package priorizadorRestricciones;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import pojos.RestriccionPerimetral;

public class ComparadorDeRestricciones implements Comparator<RestriccionPerimetral> {

	Map<RestriccionPerimetral, Double> puntajes;
	
	public ComparadorDeRestricciones(List<RestriccionPerimetral> restricciones, Criterio criterio) {
		this.puntajes = Puntaje.puntuar(restricciones, criterio);
	}
	
	public 	Map<RestriccionPerimetral, Double> getPuntajes(){
		return this.puntajes;
	}

	
	@Override
	public int compare(RestriccionPerimetral o1, RestriccionPerimetral o2) {
		// Ordena de menor a mayor.
		if (puntajes.get(o1) > puntajes.get(o2))
			return -1;
		else if (puntajes.get(o1) < puntajes.get(o2))
			return 1;
		return 0;
	}
}
