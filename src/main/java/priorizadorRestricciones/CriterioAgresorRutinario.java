package priorizadorRestricciones;

import json.JsonReaders;
import pojos.Coordenada;
import pojos.RestriccionPerimetral;
import rutinaAgresor.ControladorRutina;

public class CriterioAgresorRutinario implements Criterio {

	@Override
	public double puntuar(RestriccionPerimetral restriccion) {
		
		Coordenada ubicacionRandom = new Coordenada(0, 0);
		ubicacionRandom = JsonReaders.readCoordenada();
	
		ControladorRutina controlador = new ControladorRutina();
		
		if(controlador.estaEnRutina(restriccion.getAgresor(), ubicacionRandom, 12, 1))
			return 0;
		else 
			return 1;

	}


}
