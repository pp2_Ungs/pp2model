package priorizadorRestricciones;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pojos.RestriccionPerimetral;

public class Puntaje {
	
	public static Map<RestriccionPerimetral, Double> puntuar (List<RestriccionPerimetral> restricciones, Criterio criterio){
		Map<RestriccionPerimetral, Double> puntajes = new HashMap<RestriccionPerimetral, Double>();
		for (RestriccionPerimetral r: restricciones)
			puntajes.put(r, criterio.puntuar(r));
		return puntajes;
	}

}
