package priorizadorRestricciones;

import json.JsonReaders;
import pojos.RestriccionPerimetral;
import pojos.TipoInfraccion;

public class CriterioInfraccionesLeves implements Criterio {

	@Override
	public double puntuar(RestriccionPerimetral restriccion) {
		int cantInfracciones = dameCantidadDeInfracciones(restriccion);
		if(cantInfracciones <= 10) {
			return 0;
		}
		else if(cantInfracciones <= 20) {
			return 0.5;
	}else if(cantInfracciones <= 40) {
			return 1;}
		return 2;
	}
	
	private int dameCantidadDeInfracciones(RestriccionPerimetral restriccion) {
		return JsonReaders.readInfraccionesPorTipo(TipoInfraccion.LEVE, restriccion.getId()).size();
	}

}
