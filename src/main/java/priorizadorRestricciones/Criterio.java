package priorizadorRestricciones;

import pojos.RestriccionPerimetral;

public interface Criterio {
	
	double puntuar(RestriccionPerimetral restriccion);
	
}
