package priorizadorRestricciones;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;

public class PriorizadorDeRestricciones {
	
	public static List<RestriccionPerimetral> priorizar(List<RestriccionPerimetral> listaRestricciones, Criterio criterio){
		
		List<RestriccionPerimetral> listaRestriccionesCopia = new ArrayList<RestriccionPerimetral>(listaRestricciones);
		List<RestriccionPerimetral> listaRestriccionesPriorizadas = new ArrayList<RestriccionPerimetral>();

		listaRestriccionesPriorizadas = new ArrayList<RestriccionPerimetral>();
			
		ComparadorDeRestricciones comparador = new ComparadorDeRestricciones(listaRestriccionesCopia, criterio);
			
		Collections.sort(listaRestriccionesCopia, comparador);
			
		listaRestriccionesPriorizadas = listaRestriccionesCopia;
		
		return listaRestriccionesPriorizadas;
	}
	
}
