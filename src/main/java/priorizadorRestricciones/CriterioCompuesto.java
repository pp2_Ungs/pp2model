package priorizadorRestricciones;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import pojos.RestriccionPerimetral;

public class CriterioCompuesto implements Criterio {

	private final List<Criterio> criterios;
	
	public CriterioCompuesto(Collection<Criterio> c) {
		this.criterios = new LinkedList<Criterio>(c);
	}
	
	@Override
	public double puntuar(RestriccionPerimetral restriccion) {
		double puntajeFinal = 0;
		
		// Suma el puntaje que le da cada criterio.
		for (Criterio c: this.criterios) {
			puntajeFinal = puntajeFinal + c.puntuar(restriccion);
		}
		
		return puntajeFinal;
	}
	
}
