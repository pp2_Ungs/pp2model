package priorizadorRestricciones;

import json.JsonReaders;
import pojos.RestriccionPerimetral;
import pojos.TipoInfraccion;

public class CriterioInfraccionesGraves implements Criterio {

	@Override
	public double puntuar(RestriccionPerimetral restriccion) {
		int cantInfracciones = dameCantidadDeInfracciones(restriccion);
		if(cantInfracciones <= 3) {
			System.out.println("INFRACCIONES GRAVES 0");
			return 0;
		}
		else if(cantInfracciones <= 6) {
			return 0.5;
		}
		else if(cantInfracciones <=10) {
			return 1;
		}
		return 2;
	}
	
	private int dameCantidadDeInfracciones(RestriccionPerimetral restriccion) {
		return JsonReaders.readInfraccionesPorTipo(TipoInfraccion.GRAVE, restriccion.getId()).size();
	}

}
