package configuradorNotificaciones;

import java.util.HashMap;
import java.util.Map;

import json.JsonReaders;

public class PersonaServicios {

	Map<Integer, InfraccionServicioNotificacion> mapServiciosPersonas;
	
	public PersonaServicios (){
		mapServiciosPersonas = new HashMap<Integer, InfraccionServicioNotificacion>();
	}
	
	public void agregarPersona(Integer idPersona) {
		InfraccionServicioNotificacion infraccionNotificacion = new InfraccionServicioNotificacion();
		mapServiciosPersonas.put(idPersona, infraccionNotificacion);
	}
	
	public Map<Integer, InfraccionServicioNotificacion> getMapServiciosPersonas() {
		return mapServiciosPersonas;
	}

}
