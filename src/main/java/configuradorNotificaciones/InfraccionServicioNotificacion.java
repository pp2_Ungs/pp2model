package configuradorNotificaciones;

import java.util.HashMap;
import java.util.Map;

import pojos.ServicioNotificacion;
import pojos.TipoInfraccion;

public class InfraccionServicioNotificacion {
	
	private Map<TipoInfraccion, ServicioNotificacion> map;
	
	public InfraccionServicioNotificacion() {

		map = new HashMap<TipoInfraccion, ServicioNotificacion>();
		
		map.put(TipoInfraccion.LEVE, ServicioNotificacion.SMS);
		map.put(TipoInfraccion.MODERADA, ServicioNotificacion.SMS);
		map.put(TipoInfraccion.GRAVE, ServicioNotificacion.SMS);

	}

	public Map<TipoInfraccion, ServicioNotificacion> getMap() {
		return map;
	}
	
	public void setServicioPoInfraccion(TipoInfraccion tipo, ServicioNotificacion servicio) {
		map.put(tipo, servicio);
	}
	
	public void setInfraccionServicios(Map<TipoInfraccion, ServicioNotificacion> map) {
		this.map = map;
	}

}
