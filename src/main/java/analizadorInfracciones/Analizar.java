package analizadorInfracciones;

public interface Analizar {

	public int obtenerDistanciaParaSerGrave(int distanciaRestriccionPerimetral);

	public int obtenerDistanciaParaSerLeve(int distanciaRestriccionPerimetral);

}
