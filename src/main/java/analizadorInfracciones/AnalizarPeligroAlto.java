package analizadorInfracciones;

public class AnalizarPeligroAlto implements Analizar{

	@Override
	public int obtenerDistanciaParaSerGrave(int distanciaRestriccionPerimetral) {
		return (int) (distanciaRestriccionPerimetral * 0.20);
	}

	@Override
	public int obtenerDistanciaParaSerLeve(int distanciaRestriccionPerimetral) {
		return (int) (distanciaRestriccionPerimetral * 0.10);
	}

}
