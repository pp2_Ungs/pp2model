package analizadorInfracciones;

public class AnalizarPeligroBajo implements Analizar{

	@Override 
	public int obtenerDistanciaParaSerGrave(int distanciaRestriccionPerimetral) {
		return (int) (distanciaRestriccionPerimetral * 0.50);
	}

	@Override
	public int obtenerDistanciaParaSerLeve(int distanciaRestriccionPerimetral) {
		return (int) (distanciaRestriccionPerimetral * 0.20);
	}

}
