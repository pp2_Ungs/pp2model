package analizadorInfracciones;

import pojos.Persona;
import pojos.TipoInfraccion;

public class AnalizadorInfracciones {
	
	public static TipoInfraccion obtenerTipoInfraccion(int distanciaInfraccion,
			int distanciaRestriccionPerimetral, Persona victimario) {

		int distanciaViolada = obtenerDistanciaViolada(distanciaInfraccion, distanciaRestriccionPerimetral);
		
		int puntajePeligro = victimario.getPuntajePeligro().getPuntaje();
		Analizar analizador = (puntajePeligro<5) ? new AnalizarPeligroBajo() : new AnalizarPeligroAlto();
			
		int distanciaParaSerGrave = analizador.obtenerDistanciaParaSerGrave(distanciaRestriccionPerimetral);
		int distanciaParaSerLeve = analizador.obtenerDistanciaParaSerLeve(distanciaRestriccionPerimetral);

		return obtenerTipoInfraccion(distanciaViolada, distanciaParaSerGrave, distanciaParaSerLeve);
	}

	private static int obtenerDistanciaViolada(double distanciaInfraccion, double distanciaRestriccionPerimetral) {
		return (int) (distanciaRestriccionPerimetral - distanciaInfraccion);
	}

	private static TipoInfraccion obtenerTipoInfraccion(double distanciaViolada, double distanciaParaSerGrave,
			double distanciaParaSerLeve) {

		return (distanciaViolada <= distanciaParaSerLeve) ? TipoInfraccion.LEVE
				: ((distanciaViolada >= distanciaParaSerGrave) ? TipoInfraccion.GRAVE : TipoInfraccion.MODERADA);

	}
}
