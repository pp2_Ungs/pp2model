package notificadorGateway;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import negocio.Log;

public class EmailGateway {	
	
	public static boolean enviarMail(String email, String mensaje, String asunto) {
		
		try {
			
			// Propiedades de la conexion
			Properties props = new Properties();
			props.setProperty("mail.smtp.host", "smtp.gmail.com");
			props.setProperty("mail.smtp.starttls.enable", "true");
			props.setProperty("mail.smtp.port", "587");
			props.setProperty("mail.smtp.user", "departamentoviolenciadegenero@gmail.com");
			props.setProperty("mail.smtp.auth", "true");

			// Preparamos la sesion
			Session session = Session.getDefaultInstance(props);

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("departamentoviolenciadegenero@gmail.com"));
			
			//Reemplazar mi email por el de la persona destinataria
			message.addRecipients(Message.RecipientType.BCC,email);

			message.setSubject(asunto);
			message.setText(mensaje);
				
			// Lo enviamos.
			Transport t = session.getTransport("smtp");
			t.connect("departamentoviolenciadegenero@gmail.com", "violenciadegenero");
			t.sendMessage(message, message.getAllRecipients());

			// Cierre.
			t.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
				Log.logWarning("No se pudo establecer conexi�n con el servicio de mail");
				return false;
		}
	}

}
