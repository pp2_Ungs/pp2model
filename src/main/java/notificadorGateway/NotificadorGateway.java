package notificadorGateway;

import negocio.Log;

public class NotificadorGateway {

	public boolean notificar(String servicioDeNotificacion, String telefono, String email, String mensaje) {
		if(servicioDeNotificacion.toLowerCase().equals("whatsapp"))
			return notificarViaWhatsapp(telefono, mensaje);
		else if(servicioDeNotificacion.toLowerCase().equals("email"))
			return notificarViaEmail(email, mensaje);
		else if(servicioDeNotificacion.toLowerCase().equals("sms"))
			return notificarViaSms(telefono, mensaje);
		else {
			Log.logWarning("Servicio de notificacion "+ servicioDeNotificacion + " err�neo");
			return false;
		}
	}

	private boolean notificarViaWhatsapp(String telefono, String mensaje) {
		if(!WhatsappGateway.enviarWhatsapp(telefono, mensaje)) {
			Log.logWarning("NO SE PUDO NOTIFICAR VIA WHATSAPP");
			notificarViaSms(telefono, mensaje);
			return false;
		}
		return true;
	}
	
	private boolean notificarViaSms(String telefono, String mensaje) {
		if(!SmsGateway.enviarSms(telefono, mensaje)) {
			Log.logWarning("NO SE PUDO NOTIFICAR VIA SMS");
			return false;
		}
		return true;
	}
	
	private boolean notificarViaEmail(String email, String mensaje) {
		return EmailGateway.enviarMail(email, mensaje, "Violacion a Restriccion Perimetral");
	}
	
	
}
