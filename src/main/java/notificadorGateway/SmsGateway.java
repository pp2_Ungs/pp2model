package notificadorGateway;

import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsGateway {

	public static final String ACCOUNT_SID = "AC2559dcd066436b31b927c1e3a8c89590";
	public static final String AUTH_TOKEN = "4776b9686f9ea6875b205311fb99050e";

	public static boolean enviarSms(String numeroDestino, String mensaje) {
		try {
			Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
			Message message = Message
					.creator(new PhoneNumber("+54" + numeroDestino), new PhoneNumber("+12027336162"), mensaje)
					.create();

		} catch (ApiException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}	
}
