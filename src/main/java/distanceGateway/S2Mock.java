package distanceGateway;

import json.JsonReaders;
import negocio.CalculadorDistancias;
import pojos.Coordenada;

public class S2Mock {

	public Integer getDistance(int lat1, int lon1, int lat2, int lon2, String language, String returnType) {

		if (!language.equals("spanish"))
			return -2;

		if (!returnType.equals("json"))
			return -3;

		// return JsonReaders.readDistanciaS2(lat1, lon1, lat2, lon2);
		return CalculadorDistancias.generarDistanciaLineal(new Coordenada(lat1, lon1), new Coordenada(lat2, lon2)) + 10;

	}

}
