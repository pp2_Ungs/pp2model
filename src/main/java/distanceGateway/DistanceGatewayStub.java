package distanceGateway;

import java.util.ArrayList;
import java.util.List;

import pojos.Coordenada;

public class DistanceGatewayStub extends DistanceGateway {

	private boolean failS1 = false;
	private boolean failS2 = false;
	private boolean failS3 = false;
	
	public List<Integer> getDistancias(Coordenada origen, Coordenada destino) {
		List<Integer> distancias = new ArrayList<Integer>();
		distancias.add(s1Distancia(origen, destino));
		distancias.add(s2Distancia(origen, destino));
		distancias.add(s3Distancia(origen, destino));
		return distancias;
	}
	
	public Integer s1Distancia(Coordenada origen, Coordenada destino) {
		if(failS1)
			return null;
		return 1500;
	}

	private Integer s2Distancia(Coordenada origen, Coordenada destino) {
		if(failS2)
			return null;
		return 1510;
	}

	private Integer s3Distancia(Coordenada origen, Coordenada destino) {
		if(failS3)
			return null;
		return 1505;
	}
	
	public void setFailS1() {
		failS1 = true;
	}
	
	public void setFailS2() {
		failS2 = true;
	}
	
	public void setFailS3() {
		failS3 = true;
	}
	
	public void setFailAll() {
		setFailS1();
		setFailS2();
		setFailS3();
	}
	
}
