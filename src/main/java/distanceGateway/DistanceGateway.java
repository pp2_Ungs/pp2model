package distanceGateway;

import java.util.ArrayList;
import java.util.List;

import negocio.Log;
import pojos.Coordenada;

public class DistanceGateway {

	S1Mock s1 = new S1Mock();
	S2Mock s2 = new S2Mock();
	S3Mock s3 = new S3Mock();
	String modoTransporte = "foot";
	String unidad = "metric";
	String language = "spanish";
	String returnType = "json";

	public List<Integer> getDistancias(Coordenada origen, Coordenada destino) {
		List<Integer> distancias = new ArrayList<Integer>();
		distancias.add(s1Distancia(origen, destino));
		distancias.add(s2Distancia(origen, destino));
		distancias.add(s3Distancia(origen, destino));
		return distancias;
	}

	private Integer s1Distancia(Coordenada origen, Coordenada destino) {
		Integer distancia = s1.getDistance(origen.getX(), origen.getY(), destino.getX(), destino.getY(), unidad);
		if (distancia.equals(-1))
			Log.logWarning("Error al comunicarse con el Servicio S1. El Servicio no esta disponible");
		if (distancia.equals(-2))
			Log.logWarning("Error en la unidad solicitada al comunicarse con el Servicio S1");
		return (distancia < 0) ? null : distancia;
	}

	private Integer s2Distancia(Coordenada origen, Coordenada destino) {
		Integer distancia = s2.getDistance(origen.getX(), origen.getY(), destino.getX(), destino.getY(), language,
				returnType);
		if (distancia.equals(-1))
			Log.logWarning("Error al comunicarse con el Servicio S2. El Servicio no esta disponible");
		if (distancia.equals(-2))
			Log.logWarning("Error en el lenguaje solicitado al comunicarse con el Servicio S2");
		if (distancia.equals(-3))
			Log.logWarning("Error en el tipo de retorno solicitado al comunicarse con el Servicio S3");

		return (distancia < 0) ? null : distancia;
	}

	private Integer s3Distancia(Coordenada origen, Coordenada destino) {
		Integer distancia = s3.getDistance(origen.getX(), origen.getY(), destino.getX(), destino.getY(),
				modoTransporte);
		if (distancia.equals(-1))
			Log.logWarning("Error al comunicarse con el Servicio S3. El Servicio no esta disponible");
		if (distancia.equals(-2))
			Log.logWarning("Error en el modo de transporte solicitado al comunicarse con el Servicio S3");
		return (distancia < 0) ? null : distancia;

	}
}
