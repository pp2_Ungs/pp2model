package distanceGateway;

import json.JsonReaders;
import negocio.CalculadorDistancias;
import pojos.Coordenada;

public class S3Mock {

	public Integer getDistance(int lat1, int lon1, int lat2, int lon2, String mode) {

		if (!mode.equals("foot"))
			return -2;

		// return JsonReaders.readDistanciaS3(lat1, lon1, lat2, lon2);
		return CalculadorDistancias.generarDistanciaLineal(new Coordenada(lat1, lon1), new Coordenada(lat2, lon2)) + 5;

	}

}
