package negocio;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

public class AnalizadorDistancias {

	public static void compararDistancias(List<Integer> listaDistancias ) throws Exception {

		for(int i=0; i<listaDistancias.size(); i++) {
			if(listaDistancias.get(i) != null)
				obtenerDiferenciasGrandes(i, listaDistancias.get(i), listaDistancias);
		}
		
	}
	
	private static void obtenerDiferenciasGrandes(int indice, int distancia, List<Integer> listaDistancias) throws Exception {
		
		int maximoDiferencia = 100;
		Date date = new Date();
		DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		for(int i=indice +1; i<listaDistancias.size(); i++) {
	    	if((listaDistancias.get(i) != null) && (Math.abs(distancia - listaDistancias.get(i))) >= maximoDiferencia) {
	    		Log.logWarning(" Las distancias "+ distancia+" y "+ listaDistancias.get(i) +
	    				" tienen una diferencia de " + Math.abs(distancia - listaDistancias.get(i)) + " metros");
	    		throw new Exception();
	    	}
		}

	}

}


