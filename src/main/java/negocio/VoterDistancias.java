package negocio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VoterDistancias {

	public static Integer elegirDistancia(List<Integer> listaDistancias) {
		
		Integer distanciaElegida = null;
		int cantidadMaximaApariciones = 0;
		
		for(int i=0; i<listaDistancias.size(); i++) {
			
			if(listaDistancias.get(i) != null) {
				
				int cantidad = cantidadRepeticiones(i, listaDistancias.get(i), listaDistancias);
				
				if(cantidad > cantidadMaximaApariciones) {
					distanciaElegida = listaDistancias.get(i);
					cantidadMaximaApariciones = cantidad;
				}
				
				else if(cantidad == cantidadMaximaApariciones && listaDistancias.get(i) < distanciaElegida)
					distanciaElegida = listaDistancias.get(i);
			}
		}
		
		if(cantidadMaximaApariciones == 1) {
			try {
				AnalizadorDistancias.compararDistancias(listaDistancias);
			}
			catch(Exception e){
				return null;
			}
		}
		return distanciaElegida;
	}

	private static int cantidadRepeticiones(int indice, int distancia, List<Integer> listaDistancias) {

		int contador = 0;

		for (int i = indice; i < listaDistancias.size(); i++) {
			if (listaDistancias.get(i) != null && distancia == listaDistancias.get(i)) {
				contador++;
			}
		}

		return contador;
	}

	/*
	 * private static Integer elegirDistancia(Map<String,Integer> listaDistancias) {
	 * 
	 * int distanciaElegida = 0; int cantidadMaximaRepeticiones = 0;
	 * 
	 * for (String key : listaDistancias.keySet()) { Integer value =
	 * listaDistancias.get(key); int cantidad = cantidadRepeticiones(value,
	 * listaDistancias);
	 * 
	 * if(cantidad > cantidadMaximaRepeticiones) { distanciaElegida = value;
	 * cantidadMaximaRepeticiones = cantidad; }
	 * 
	 * else if(cantidad == cantidadMaximaRepeticiones && value < distanciaElegida)
	 * distanciaElegida = value;
	 * 
	 * }
	 * 
	 * return distanciaElegida;
	 * 
	 * }
	 * 
	 * private static int cantidadRepeticiones(int distancia, Map<String,Integer>
	 * listaDistancias) {
	 * 
	 * int contador = 0;
	 * 
	 * for (String key : listaDistancias.keySet()) { Integer value =
	 * listaDistancias.get(key);
	 * 
	 * if(distancia == value) { contador++; } }
	 * 
	 * return contador; }
	 */

}
