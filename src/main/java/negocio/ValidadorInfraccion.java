package negocio;

public class ValidadorInfraccion {

	public static boolean validarDistanciaInfraccion(int distanciaRestriccion, int distanciaInfraccion) {

		String msj = (distanciaInfraccion > distanciaRestriccion)
				? "No es una infraccion porque la distancia es mayor que la de la Restriccion perimetral"
				: "";

		msj = (distanciaInfraccion < 0) ? msj+"\n Las distancias no pueden ser negativas" : msj+"";
		
		if (!msj.isEmpty()) {
			Log.logWarning(msj);
			return false;
		}
		return true;
		}

}
