package negocio;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {

	private static Logger LOGGER = Logger.getLogger("Log");


	public static void logWarning(String msj) {
		FileHandler fh = null;
		try {
			fh = new FileHandler("Log.log", true);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}  
        LOGGER.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);  

        LOGGER.warning(msj);
	}	


}
