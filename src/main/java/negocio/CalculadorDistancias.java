package negocio;

import pojos.Coordenada;

public class CalculadorDistancias {

	public static int generarDistanciaLineal(Coordenada c1, Coordenada c2) {

		double distancia = Math.sqrt((Math.pow(c2.getX() - c1.getX(), 2)) + (Math.pow(c2.getY() - c1.getY(), 2)));

		return (int) Math.round(distancia);
	}

}
