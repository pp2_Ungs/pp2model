package json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import negocio.Log;
import pojos.Coordenada;
import pojos.Infraccion;
import pojos.RestriccionPerimetral;
import pojos.TipoInfraccion;
import pojos.UbicacionDiaHora;

public class JsonUpdaters {

	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	public static boolean updateInfraccion(Infraccion infraccion) {
		HashMap<Integer, List<Infraccion>> infraccionesLeidas = JsonReaders.readInfracciones();
		int idPersona = infraccion.getRestriccionPerimetral().getAgresor().getIdPersona();
		List<Infraccion> infraccionesPersona = infraccionesLeidas.get(idPersona);
		
		int indice = 0;
		for (Infraccion i : infraccionesPersona) {
			if (i.getId() == infraccion.getId()) {
				infraccionesPersona.set(indice, infraccion);
			
			}
			indice++;
		}
		infraccionesLeidas.put(idPersona, infraccionesPersona);
				
		return JsonWriters.writePersonasInfracciones(infraccionesLeidas);
		
	}
}
