package json;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pojos.Coordenada;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;

public class CrearMocksJson {

	static Persona A = new Persona("A", "A", "123456", RolEnRestriccion.AGRESOR, 3, "", "");
	static Persona B = new Persona("B", "B", "987654321", RolEnRestriccion.AGRESOR, 8, "", "");
	static Persona C = new Persona("C", "C", "987654321", RolEnRestriccion.AGRESOR, 9, "", "");

	static RestriccionPerimetral R1 = new RestriccionPerimetral(null, 100, 1, null, A, new Coordenada(0, 0));
	static RestriccionPerimetral R2 = new RestriccionPerimetral(null, 500, 2, null, B, new Coordenada(0, 0));
	static RestriccionPerimetral R3 = new RestriccionPerimetral(null, 100, 3, null, C, new Coordenada(0, 0));

	static Infraccion infB;
	static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static Date fecha;

	public CrearMocksJson() {
		Persona D = new Persona("D", "D", "654", RolEnRestriccion.VICTIMA, 50, "", "");
		Persona E = new Persona("E", "E", "987", RolEnRestriccion.VICTIMA, 51, "", "");
		Persona F = new Persona("F", "F", "321", RolEnRestriccion.VICTIMA, 52, "", "");
		List<Persona> personas = new ArrayList<>();
		personas.add(D);
		personas.add(E);
		personas.add(F);
		
		
		Writer writers;
		try {
			writers = new FileWriter("personas.json");
			writers.write(gson.toJson(personas));
			writers.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static void setFecha() {
		try {
			fecha = sdf.parse("2019-05-20 00:59:55");
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	public static void userStory5Criterio1() {
		setFecha();
		HashMap<Integer, List<Infraccion>> infracciones = new HashMap<Integer, List<Infraccion>>();
		infracciones.put(A.getIdPersona(), null);

		List<Infraccion> infraccionesB = new ArrayList<Infraccion>();
		infB = new Infraccion(1, fecha, 499, R2);

		infraccionesB.add(infB);
		infracciones.put(B.getIdPersona(), infraccionesB);

		write(infracciones);
	}

	public static void userStory5Criterio2() {
		setFecha();
		HashMap<Integer, List<Infraccion>> infracciones = new HashMap<Integer, List<Infraccion>>();
		List<Infraccion> infraccionesA = new ArrayList<Infraccion>();
		Infraccion infA = new Infraccion(95, fecha, 10, R1);
		infraccionesA.add(infA);
		infracciones.put(A.getIdPersona(), infraccionesA);

		List<Infraccion> infraccionesB = new ArrayList<Infraccion>();
		Infraccion infB2 = new Infraccion(64, fecha, 350, R2);
		infB = new Infraccion(1, fecha, 499, R2);

		infraccionesB.add(infB);
		infraccionesB.add(infB2);
		infracciones.put(B.getIdPersona(), infraccionesB);

		write(infracciones);
	}

	public static void userStory5Criterio3() {
		setFecha();
		HashMap<Integer, List<Infraccion>> infracciones = new HashMap<Integer, List<Infraccion>>();
		List<Infraccion> infraccionesA = new ArrayList<Infraccion>();
		Infraccion infA = new Infraccion(95, fecha, 10, R1);
		Infraccion infA2 = new Infraccion(95, fecha, 65, R1);

		infraccionesA.add(infA);
		infraccionesA.add(infA2);
		infracciones.put(A.getIdPersona(), infraccionesA);

		List<Infraccion> infraccionesB = new ArrayList<Infraccion>();
		Infraccion infB2 = new Infraccion(64, fecha, 350, R2);
		Infraccion infB3 = new Infraccion(64, fecha, 499, R2);
		infB = new Infraccion(1, fecha, 499, R2);

		infraccionesB.add(infB);
		infraccionesB.add(infB2);
		infraccionesB.add(infB3);
		infracciones.put(B.getIdPersona(), infraccionesB);

		write(infracciones);
	}

	public static void userStory1IT2Criterio1() {
		setFecha();
		HashMap<Integer, List<Infraccion>> infracciones = new HashMap<Integer, List<Infraccion>>();
		List<Infraccion> infraccionesA = new ArrayList<Infraccion>();

		Infraccion infA = new Infraccion(95, fecha, 10, R1);
		Infraccion infA2 = new Infraccion(95, fecha, 10, R1);
		Infraccion infA3 = new Infraccion(95, fecha, 10, R1);
		Infraccion infA4 = new Infraccion(95, fecha, 10, R1);

		Infraccion infA5 = new Infraccion(90, fecha, 70, R1);
		Infraccion infA6 = new Infraccion(90, fecha, 70, R1);
		Infraccion infA7 = new Infraccion(90, fecha, 70, R1);

		infraccionesA.add(infA);
		infraccionesA.add(infA2);
		infraccionesA.add(infA3);
		infraccionesA.add(infA4);
		infraccionesA.add(infA5);
		infraccionesA.add(infA6);
		infraccionesA.add(infA7);

		infracciones.put(A.getIdPersona(), infraccionesA);

		List<Infraccion> infraccionesB = new ArrayList<Infraccion>();
		infB = new Infraccion(1, fecha, 499, R2);

		infraccionesB.add(infB);
		infracciones.put(B.getIdPersona(), infraccionesB);

		List<Infraccion> infraccionesC = new ArrayList<Infraccion>();

		Infraccion infC = new Infraccion(95, fecha, 10, R3);
		Infraccion infC2 = new Infraccion(95, fecha, 10, R3);
		Infraccion infC3 = new Infraccion(95, fecha, 10, R3);

		infraccionesC.add(infC);
		infraccionesC.add(infC2);
		infraccionesC.add(infC3);

		infracciones.put(C.getIdPersona(), infraccionesC);

		write(infracciones);
	}

	public static void muchasInfracciones() {
		setFecha();
		HashMap<Integer, List<Infraccion>> infracciones = new HashMap<Integer, List<Infraccion>>();
		List<Infraccion> infraccionesA = new ArrayList<Infraccion>();

		for (int i = 0; i < 9; i++) {
			Infraccion infA = new Infraccion(100, fecha, 10, R1);
			Infraccion infA2 = new Infraccion(200, fecha, 70, R1);
			Infraccion infA3 = new Infraccion(300, fecha, 99, R1);

			infraccionesA.add(infA);
			infraccionesA.add(infA2);
			infraccionesA.add(infA3);

		}
		for (int i = 0; i < 5; i++) {
			Infraccion infA3 = new Infraccion(300, fecha, 99, R1);
			infraccionesA.add(infA3);
		}
		infracciones.put(A.getIdPersona(), infraccionesA);

		List<Infraccion> infraccionesB = new ArrayList<Infraccion>();
		for (int i = 0; i < 35; i++) {
			Infraccion infA3 = new Infraccion(300, fecha, 499, R2);
			infraccionesB.add(infA3);
		}
		for (int i = 0; i < 15; i++) {
			Infraccion infA3 = new Infraccion(200, fecha, 350, R2);
			infraccionesB.add(infA3);
		}
		infracciones.put(B.getIdPersona(), infraccionesB);

		List<Infraccion> infraccionesC = new ArrayList<Infraccion>();
		for (int i = 0; i < 50; i++) {
			Infraccion infA = new Infraccion(100, fecha, 10, R3);
			Infraccion infA2 = new Infraccion(200, fecha, 70, R3);
			Infraccion infA3 = new Infraccion(300, fecha, 99, R3);
			infraccionesC.add(infA3);
			infraccionesC.add(infA2);
			infraccionesC.add(infA);
		}

		infracciones.put(C.getIdPersona(), infraccionesC);

		write(infracciones);

	}

	private static void write(HashMap<Integer, List<Infraccion>> infracciones) {
		Writer writers;
		try {
			writers = new FileWriter("personasInfraccions.json");
			writers.write(gson.toJson(infracciones));
			writers.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
