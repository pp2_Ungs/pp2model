package json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import negocio.Log;
import pojos.Coordenada;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.TipoInfraccion;
import pojos.UbicacionDiaHora;

public class JsonReaders {

	public static HashMap<TipoInfraccion, Integer> readReglasTipoInfraccion() {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Reader reader = readArch("reglasPuntajes.json");
		Type type = new TypeToken<HashMap<TipoInfraccion, Integer>>() {
		}.getType();
		HashMap<TipoInfraccion, Integer> result = gson.fromJson(reader, type);

		return result;

	}

	public static List<Infraccion> readInfraccionesPorTipo(TipoInfraccion tipoInfraccion, int idRestriccion) {
		List<Infraccion> infracciones = readInfraccionesPorRestriccion(idRestriccion);
		List<Infraccion> retorno = new ArrayList<Infraccion>();
		for (Infraccion i : infracciones) {
			if (i.getTipoInfraccion().equals(tipoInfraccion))
				retorno.add(i);
		}
		return retorno;
	}

	public static HashMap<Integer, List<Infraccion>> readInfracciones() {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Reader reader = readArch("personasInfraccions.json");
		Type type = new TypeToken<HashMap<Integer, List<Infraccion>>>() {
		}.getType();
		HashMap<Integer, List<Infraccion>> result = gson.fromJson(reader, type);

		return result;

	}

	public static List<Infraccion> readInfraccionesPorRestriccion(int idRestriccion) {
		HashMap<Integer, List<Infraccion>> infracciones = readInfracciones();
		List<Infraccion> infraccionesPorRestriccion = new ArrayList<Infraccion>();
		infracciones.forEach((key, values) -> {
			for (Infraccion i : values) {
				if (i.getRestriccionPerimetral().getId() == idRestriccion)
					infraccionesPorRestriccion.add(i);
			}
		});
		return infraccionesPorRestriccion;
	}

	public static List<Infraccion> readInfraccionesPorPersona(int idPersona) {
		HashMap<Integer, List<Infraccion>> result = readInfracciones();
		return result.get(idPersona);
	}

	public static List<RestriccionPerimetral> readRestriccionesPerimetrales() {
		HashMap<Integer, List<Infraccion>> infracciones = readInfracciones();
		HashMap<Integer, RestriccionPerimetral> noDuplicados = new HashMap<Integer, RestriccionPerimetral>();
		List<RestriccionPerimetral> restricciones = new ArrayList<RestriccionPerimetral>();
		infracciones.forEach((key, values) -> {
			for (Infraccion i : values) {
				noDuplicados.put(i.getRestriccionPerimetral().getId(), i.getRestriccionPerimetral());
			}
		});
		noDuplicados.forEach((key, values) -> restricciones.add(values));
		return restricciones;
	}

	public static Coordenada readCoordenada() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Reader reader = readArch("coordenadasRandom.json");
		Type type = new TypeToken<List<Coordenada>>() {
		}.getType();
		List<Coordenada> result = gson.fromJson(reader, type);

		int rnd = new Random().nextInt(result.size() - 1);

		return result.get(rnd);
	}

	public static List<UbicacionDiaHora> readHistorialUbicaionAgresores() {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Reader reader = readArch("historialAgresor.json");
		Type type = new TypeToken<List<UbicacionDiaHora>>() {
		}.getType();
		List<UbicacionDiaHora> result = gson.fromJson(reader, type);

		return result;

	}
	
	public static List<Persona> readPersonas() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Reader reader = readArch("personas.json");
		Type type = new TypeToken<List<Persona>>() {
		}.getType();
		List<Persona> result = gson.fromJson(reader, type);

		return result;
	}

	public static Reader readArch(String nombreArchivo) {
		Reader reader = null;
		try {
			reader = new FileReader(nombreArchivo);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Log.logWarning("No hay archivo llamado"+nombreArchivo);
		}
		return reader;
	}
}
