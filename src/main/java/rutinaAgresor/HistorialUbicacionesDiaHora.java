package rutinaAgresor;

import java.util.ArrayList;
import java.util.List;

import pojos.Coordenada;
import pojos.UbicacionDiaHora;

public class HistorialUbicacionesDiaHora {

	private List<UbicacionDiaHora> historialAgresorXdiaHora;
	
	public HistorialUbicacionesDiaHora() {
		historialAgresorXdiaHora = new ArrayList<UbicacionDiaHora>();
	}
	
	public void insertarUbicacion(UbicacionDiaHora ubicacion) {
		historialAgresorXdiaHora.add(ubicacion);
	}
	
	//obtengo una ubicacion promedio para la zona mas repetida para esa hora
	public Coordenada dameUbicacionHabitual() {

		Coordenada ubicacionMasRepetida = new Coordenada(0, 0);
		int maximo = 0;
		
		for(Coordenada ubicacion : coordenadasHoraDia()) {
			if(contadorUbicacionRepetida(ubicacion) >= maximo) {
				ubicacionMasRepetida = ubicacion;
				maximo = contadorUbicacionRepetida(ubicacion);
			}
		}
		
		//70 por los dias habiles de la semana
		if(coordenadasHoraDia().size()!= 0 && ((maximo*100)/coordenadasHoraDia().size()) >= 70)
			return calcularUbicacionPromedio(ubicacionMasRepetida);

		return null;
	}
	
	//saca un promedio para las ubicaciones dentro de una misma area
	private Coordenada calcularUbicacionPromedio(Coordenada ubicacionMedio) {
		
		List<Coordenada> ubicacionesDentroDeArea = new ArrayList<Coordenada>();
		Coordenada ubicacionPromedio = null;
		int x = 0;
		int y = 0;
		AreaDeUbicacion area = new AreaDeUbicacion(ubicacionMedio, 10);
		
		for(Coordenada ubicacion : coordenadasHoraDia()) {
			if(area.estaEnArea(ubicacion))
				ubicacionesDentroDeArea.add(ubicacion);
		}
		
		for(Coordenada ubicacionParaSumatoria : ubicacionesDentroDeArea ) {
			x = x + ubicacionParaSumatoria.getX();
			y = y + ubicacionParaSumatoria.getY();
		}
		
		ubicacionPromedio = new Coordenada(x/ubicacionesDentroDeArea.size(), y/ubicacionesDentroDeArea.size());
		
		return ubicacionPromedio;
	}

	
	//cuenta cuantas ubicaciones en el historial estan dentro del area para esa hora
	private int contadorUbicacionRepetida(Coordenada ubicacion) {

		int contador = 0;
		AreaDeUbicacion area = new AreaDeUbicacion(ubicacion, 10);
		
		for(Coordenada ubicacionActual : coordenadasHoraDia()) {
			if(area.estaEnArea(ubicacionActual))
				contador++;
		}
		
		return contador;
	}

	//devuelve una lista de coordenadas del agresor
	private List<Coordenada> coordenadasHoraDia(){
		
		List<Coordenada> listaCoordenadas = new ArrayList<Coordenada>();
		
		for(UbicacionDiaHora ubicacionXHora : historialAgresorXdiaHora) {
			listaCoordenadas.add(ubicacionXHora.getUbicacion());
		}
		
		return listaCoordenadas;
	}

}
