package rutinaAgresor;

import pojos.Coordenada;

public class AreaDeUbicacion {

	private Coordenada coordenadaMedia;
	private int distancia;
	
	public AreaDeUbicacion(Coordenada coordenadaMedia, int distancia) {
		
		this.coordenadaMedia = coordenadaMedia;
		this.distancia = distancia;
		
	}
	
	public boolean estaEnArea(Coordenada coordenada) {
		
		if(coordenada!=null && coordenadaMedia!=null)
			if(coordenada.getX() <= coordenadaMedia.getX() + distancia && coordenada.getX() >= coordenadaMedia.getX() - distancia
					&& coordenada.getY() <= coordenadaMedia.getY() + distancia && coordenada.getY() >= coordenadaMedia.getY() - distancia)		
				return true;
		
		return false;
	}

	public Coordenada getCoordenadaMedia() {
		return coordenadaMedia;
	}

	public int getDistancia() {
		return distancia;
	}
	
}
