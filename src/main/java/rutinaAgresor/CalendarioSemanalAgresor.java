package rutinaAgresor;

import java.util.ArrayList;
import java.util.List;

import json.JsonReaders;
import pojos.Coordenada;
import pojos.Persona;
import pojos.UbicacionDiaHora;

public class CalendarioSemanalAgresor {
	
	private Persona agresor;
	private HistorialUbicacionesDiaHora semana[][];
	
	public CalendarioSemanalAgresor(Persona agresor) {

		this.agresor = agresor;
		this.semana = new HistorialUbicacionesDiaHora[24][7];
		
		for(int i = 0; i<24; i++) {
			for(int j = 0; j<7; j++) {
				semana[i][j] = new HistorialUbicacionesDiaHora();
			}
		}
		
		cargarHistorial();

	}
	
	public void cargarHistorial() {
		
		List<UbicacionDiaHora> lista = new ArrayList<UbicacionDiaHora>();
		lista = JsonReaders.readHistorialUbicaionAgresores();
		
		for(UbicacionDiaHora historialDeAgresores : lista) {
			if(agresor.getIdPersona() == historialDeAgresores.getAgresor().getIdPersona())
				semana[historialDeAgresores.getHora()][historialDeAgresores.getDiaDeSemana()].insertarUbicacion(historialDeAgresores);
		}
		
	}
	
	public boolean estaDentroDeAreaPromedio(Coordenada ubicacion, int hora, int dia) {
		
		AreaDeUbicacion areaDeUbicacionPromedio = new AreaDeUbicacion(semana[hora][dia].dameUbicacionHabitual(), 10);
		if(areaDeUbicacionPromedio.estaEnArea(ubicacion))
			return true;
		return false;

	}
	
	public Persona getAgresor() {
		return agresor;
	}
}
