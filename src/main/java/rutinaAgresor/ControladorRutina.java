package rutinaAgresor;

import java.util.ArrayList;
import java.util.List;

import json.JsonReaders;
import pojos.Coordenada;
import pojos.Persona;
import pojos.RolEnRestriccion;
import pojos.UbicacionDiaHora;

public class ControladorRutina {
	
	private List<CalendarioSemanalAgresor> calendarioAgresores;
	
	public ControladorRutina() {
		calendarioAgresores = new ArrayList<CalendarioSemanalAgresor>();
		crearHistoriales();
	}
	
	public void crearHistoriales() {
		
		List<UbicacionDiaHora> lista = new ArrayList<UbicacionDiaHora>();
		lista = JsonReaders.readHistorialUbicaionAgresores();
		
		for(UbicacionDiaHora historialDeAgresores : lista) {
			if(!existeCalendario(historialDeAgresores.getAgresor())) {
				CalendarioSemanalAgresor calenarioAgresor = new CalendarioSemanalAgresor(historialDeAgresores.getAgresor());
				this.calendarioAgresores.add(calenarioAgresor);
			}
		}
		
	}
	
	public boolean existeCalendario(Persona agresor) {
		
		for(CalendarioSemanalAgresor calendarioAgresor: calendarioAgresores) {
			if(calendarioAgresor.getAgresor().getIdPersona() == agresor.getIdPersona())
				return true;
		}
		return false;
		
	}

	public boolean estaEnRutina (Persona agresor, Coordenada ubicacion, int hora, int dia) {
		
		for(CalendarioSemanalAgresor calendarioAgresor: calendarioAgresores) {
			if(calendarioAgresor.getAgresor().getIdPersona() == agresor.getIdPersona())
				if(!calendarioAgresor.estaDentroDeAreaPromedio(ubicacion, hora, dia))
					return false;
		}
		
		return true;		
	}

}
