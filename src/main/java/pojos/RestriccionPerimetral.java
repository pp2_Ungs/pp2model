package pojos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RestriccionPerimetral {

	private Date fechaSentencia;
	private int distancia;
	private int id;
	private Persona victima;
	private Persona agresor;
	private List<Persona> contactosVictima;
	private Coordenada ubicacion;

	public RestriccionPerimetral(Date fechaSentencia, int distancia, int id, Persona victima, Persona agresor, Coordenada ubicacion) {
		this.fechaSentencia = fechaSentencia;
		this.distancia = distancia;
		this.id = id;
		this.victima = victima;
		this.agresor = agresor;
		this.contactosVictima = new ArrayList<Persona>();
		this.ubicacion = ubicacion;
	}

	public Date getFechaSentencia() {
		return fechaSentencia;
	}

	public int getDistancia() {
		return distancia;
	}

	public int getId() {
		return id;
	}
	
	public Persona getVictima() {
		return victima;
	}
	
	public Persona getAgresor() {
		return agresor;
	}

	public List<Persona> getContactosVictima() {
		return contactosVictima;
	}

	public void agregarContactoVictima(Persona persona) {
		contactosVictima.add(persona);
	}

	public Coordenada getUbicacion() {
		return ubicacion;
	}

	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		return "RESTRICCIONES PERIMETRALES:\n                             id = " + this.id + ",   distancia = " + this.distancia;
	}

}
