package pojos;

public class UbicacionDiaHora {
	
	private int id;
	private Persona agresor;
	private Coordenada ubicacion;
	private int diaDeSemana;
	private int hora;
	
	public UbicacionDiaHora(int id, Persona agresor, Coordenada ubicacion, int diaDeSemana, int hora) {
		
		this.id = id;
		this.agresor = agresor;
		this.ubicacion = ubicacion;
		this.diaDeSemana = diaDeSemana;
		this.hora = hora;
	}

	public int getId() {
		return id;
	}

	public Persona getAgresor() {
		return agresor;
	}

	public Coordenada getUbicacion() {
		return ubicacion;
	}

	public int getDiaDeSemana() {
		return diaDeSemana;
	}

	public int getHora() {
		return hora;
	}

}


