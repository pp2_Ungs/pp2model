package pojos;

import java.util.HashMap;
import java.util.Map;

public class Persona {

	String nombre;
	String apellido;
	String DNI;
	RolEnRestriccion rolPersona;
	PuntajePeligro puntajePeligro;
	int idPersona;
	String email;
	String numeroTelefono;
	
	public Persona(String nombre, String apellido, String dNI, RolEnRestriccion rolPersona, int idPersona, String email, String numero) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.DNI = dNI;
		this.rolPersona = rolPersona;
		this.idPersona = idPersona;
		this.email = email;
		this.numeroTelefono = numero;
		this.puntajePeligro = new PuntajePeligro(idPersona);
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getDNI() {
		return DNI;
	}

	public RolEnRestriccion getRolPersona() {
		return rolPersona;
	}

	public PuntajePeligro getPuntajePeligro() {
		return puntajePeligro;
	}

	public int getIdPersona() {
		return idPersona;
	}
	
	public String getEmail() {
		return email;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}
	
}
