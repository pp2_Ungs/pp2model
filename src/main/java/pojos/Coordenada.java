package pojos;

public class Coordenada {
	
	private int x;
	private int y;
	
	public Coordenada(int x, int y) {
		this.x = x;
		this.y = y;		
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "( " + x + " , "+ y + " )";
	}
	
	public boolean equals(Coordenada c) {
		return (this.x == c.getX() && this.y == c.getY());
	}

}
