package pojos;

import java.text.SimpleDateFormat;
import java.util.Date;

import analizadorInfracciones.AnalizadorInfracciones;
import negocio.ValidadorInfraccion;

public class Infraccion {

	private int id;
	private Date fecha;
	private int distancia;
	private TipoInfraccion tipoInfraccion;
	private RestriccionPerimetral restriccionPerimetral;

	public Infraccion(int id, Date fecha, int distancia, RestriccionPerimetral restriccionPerimetral) {
		if (!ValidadorInfraccion.validarDistanciaInfraccion(restriccionPerimetral.getDistancia(), distancia)) {
			try {
				throw new Exception("Infraccion invalida");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.id = id;
		this.fecha = fecha;
		this.distancia = distancia;
		this.tipoInfraccion = AnalizadorInfracciones.obtenerTipoInfraccion(distancia,
				restriccionPerimetral.getDistancia(), restriccionPerimetral.getAgresor());
		this.restriccionPerimetral = restriccionPerimetral;
	}

	public Date getFecha() {
		return fecha;
	}

	public double getDistancia() {
		return distancia;
	}

	public TipoInfraccion getTipoInfraccion() {
		return tipoInfraccion;
	}

	public int getId() {
		return id;
	}

	public RestriccionPerimetral getRestriccionPerimetral() {
		return restriccionPerimetral;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public void setDistancia(int distancia) {
		this.distancia = distancia;
	}

	public void setTipoInfraccion(TipoInfraccion tipoInfraccion) {
		this.tipoInfraccion = tipoInfraccion;
	}

	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		return "INFRACCION:\n              id = " + this.id + ",   fecha = " + dateFormat.format(this.fecha)
				+ ",   distancia = " + this.distancia + ",   tipo = " + this.tipoInfraccion
				+ ",   id Restriccion Perimetral = " + this.restriccionPerimetral.getId();

	}

	public boolean equals(Infraccion otra) {
		return otra.getId() == this.id && 
				otra.getDistancia() == this.distancia && 
				otra.getFecha().equals(this.fecha) &&
				otra.getTipoInfraccion().equals(this.tipoInfraccion) &&
				otra.getRestriccionPerimetral().getId() == this.getRestriccionPerimetral().getId();
	}
}
