package pojos;

import calculadorPuntajePeligro.CalculadorPuntajePeligro;

public class PuntajePeligro {
	
	int puntaje;
	
	public PuntajePeligro(int idPersona) {
		puntaje = 0;
	}

	private int calcularPuntaje(int idPersona) {
		return CalculadorPuntajePeligro.calcularPuntaje(idPersona);
	}
	
	public void actualizarPuntaje(int idPersona) {
		this.puntaje = calcularPuntaje(idPersona);
	}

	public int getPuntaje() {
		return puntaje;
	}
	
}
