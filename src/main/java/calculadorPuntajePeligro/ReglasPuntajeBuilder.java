package calculadorPuntajePeligro;

import java.util.HashMap;
import pojos.TipoInfraccion;

public abstract class ReglasPuntajeBuilder {
	
	public int puntajeInfraccionLeve;
	public int puntajeInfraccionModerada;
	public int puntajeInfraccionGrave;
	HashMap<TipoInfraccion, Integer> reglas;

	
	public abstract void buildReglas();
	
}
