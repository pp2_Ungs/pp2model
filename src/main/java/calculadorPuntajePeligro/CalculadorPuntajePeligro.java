package calculadorPuntajePeligro;

import java.util.ArrayList;
import json.JsonReaders;
import pojos.Infraccion;
import pojos.TipoInfraccion;

public class CalculadorPuntajePeligro {

	public static int calcularPuntaje(int idPersona) {
		ArrayList<Infraccion> infracciones = (ArrayList<Infraccion>) JsonReaders.readInfraccionesPorPersona(idPersona);

		if(infracciones==null)
			return 0;
		
		int cantInfraccionesLeves = cantInfraccionesPorTipo(infracciones, TipoInfraccion.LEVE);
		int cantInfraccionesModeradas = cantInfraccionesPorTipo(infracciones, TipoInfraccion.MODERADA);
		int cantInfraccionesGraves = cantInfraccionesPorTipo(infracciones, TipoInfraccion.GRAVE);

		ReglasPuntajeBuilder reglas = new PuntosPorTipoInfraccion();
		reglas.buildReglas();

		int sumaPuntos = cantInfraccionesGraves * reglas.puntajeInfraccionGrave
				+ cantInfraccionesLeves * reglas.puntajeInfraccionLeve
				+ cantInfraccionesModeradas * reglas.puntajeInfraccionModerada;

		return (int) Math.ceil((double)sumaPuntos / infracciones.size());
	}

	private static int cantInfraccionesPorTipo(ArrayList<Infraccion> infracciones, TipoInfraccion tipo) {
		int cant = 0;
		for (Infraccion i : infracciones) {
			if (i.getTipoInfraccion().equals(tipo))
				cant++;
		}
		return cant;
	}

}
