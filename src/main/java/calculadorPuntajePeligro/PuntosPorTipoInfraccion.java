package calculadorPuntajePeligro;

import json.JsonReaders;
import pojos.TipoInfraccion;

public class PuntosPorTipoInfraccion extends ReglasPuntajeBuilder{
	

	@Override
	public void buildReglas() {
		reglas = JsonReaders.readReglasTipoInfraccion();
		
		puntajeInfraccionLeve = reglas.get(TipoInfraccion.LEVE);
		puntajeInfraccionModerada = reglas.get(TipoInfraccion.MODERADA);
		puntajeInfraccionGrave = reglas.get(TipoInfraccion.GRAVE);
	}		
	
}
