package notificador;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pojos.Coordenada;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;
import pojos.TipoInfraccion;

public class SelectorDeNotificaciones {

	// 3= GRAVE, 2 = MODERADA , 1 = LEVE
	public int notificar(Infraccion infraccion, RestriccionPerimetral restriccion) {

		NotificadorStrategy notificador = null;
		int ret = 0;

		if (restriccion.getAgresor().getPuntajePeligro().getPuntaje() >= 5) {
			if (infraccion.getTipoInfraccion() == TipoInfraccion.GRAVE
					|| infraccion.getTipoInfraccion() == TipoInfraccion.MODERADA) {
				notificador = new NotificacionGrave();
				ret = 3;
			} else {
				notificador = new NotificacionModerada();
				ret = 2;
			}
		} else if (infraccion.getTipoInfraccion() == TipoInfraccion.GRAVE) {
			notificador = new NotificacionGrave();
			ret = 3;
		} else if (infraccion.getTipoInfraccion() == TipoInfraccion.MODERADA) {
			notificador = new NotificacionModerada();
			ret = 2;
		} else {
			notificador = new NotificacionLeve();
			ret = 1;
		}

		notificador.notificarReceptores(infraccion, restriccion);
		return ret;
	}
	
/*	  
	  public static void main(String[] args) {

		  Persona agresor = new Persona("A", "A", "2323", RolEnRestriccion.AGRESOR, 0, "", "");
		  Persona victima = new Persona("B", "B", "2222", RolEnRestriccion.VICTIMA, 1, "", "");
		  Persona contactovictima = new Persona("C", "C", "23222", RolEnRestriccion.CONTACTOVICTIMA, 2, "", "");
		  RestriccionPerimetral restriccion = new RestriccionPerimetral(null, 100, 0, victima, agresor, new Coordenada(0, 0));
		  Map<Integer, String> listaServicios = new HashMap<Integer, String>();
		  listaServicios.put(2, "Email");
		  listaServicios.put(1, "Whatsapp");

		  Map<Integer, String> listaServiciosVictima = new HashMap<Integer, String>();
		  listaServiciosVictima.put(1, "Email");
		  listaServiciosVictima.put(2, "Whatsapp");

		  restriccion.agregarContactoVictima(contactovictima);
		  agresor.setServiciosDeNotificaciones(listaServicios);
		  victima.setServiciosDeNotificaciones(listaServiciosVictima);
		  
		  Infraccion infraccion = new Infraccion(0, null, 95, restriccion);
		  
		  SelectorDeNotificaciones n = new SelectorDeNotificaciones();
		  
		  n.notificar(infraccion, restriccion);
	  }
*/	 
	  

}
