package notificador;

import java.util.List;

import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;

public interface NotificadorStrategy {
	
	public void notificarReceptores(Infraccion infraccion, RestriccionPerimetral restriccion);
	
}
