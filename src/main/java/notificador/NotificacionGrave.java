package notificador;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import pojos.RolEnRestriccion;

public class NotificacionGrave implements NotificadorStrategy {

	@Override
	public void notificarReceptores(Infraccion infraccion, RestriccionPerimetral restriccion) {
		System.out.println("NOTIFICO GRAVEMENTE");
		System.out.println("Notificar a Victima, contactos de victima, agresor y policia");
		System.out.println("El victimario" + restriccion.getAgresor().getApellido() + " cometio una infraccion " + infraccion.getTipoInfraccion());
		
		List<Persona> destinatarios = new ArrayList<Persona>();

		//Metodo que agrega destinatarios a la lista
		agregarDestinatarios(restriccion, destinatarios);
		
		for(Persona destinatario: destinatarios) {
	//		TreeMap<Integer, String> listaservicios = new TreeMap<Integer, String>(destinatario.getServiciosDeNotificaciones());
			
		//	System.out.println("Notificado a " + destinatario.getApellido() + " por = " + listaservicios);

			//		EnviarMail.enviarMail(infraccion, restriccion.getAgresor(), restriccion.getVictima(), destinatario);
		}
	}
	
	private void agregarDestinatarios(RestriccionPerimetral restriccion, List<Persona> destinatarios) {
		
		Persona policia = new Persona("Policia", "Policia", "", null, 1111, "", "");
		
		destinatarios.add(restriccion.getAgresor());
		destinatarios.add(restriccion.getVictima());
		destinatarios.add(policia);

		if(restriccion.getContactosVictima().size() >= 0)
			for(Persona persona: restriccion.getContactosVictima()){
				destinatarios.add(persona);
			}
	}


}
