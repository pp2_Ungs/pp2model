package notificador;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pojos.Coordenada;
import pojos.Infraccion;
import pojos.Persona;
import pojos.RestriccionPerimetral;

public class NotificacionLeve implements NotificadorStrategy {

	@Override
	public void notificarReceptores(Infraccion infraccion, RestriccionPerimetral restriccion) {
		System.out.println("NOTIFICO LEVEMENTE");
		System.out.println("Notificar a Victima y agresor");
		System.out.println("El victimario " + restriccion.getAgresor().getApellido() + " cometio una infraccion " + infraccion.getTipoInfraccion());
		
		List<Persona> destinatarios = new ArrayList<Persona>();
		
		agregarDestinatarios(restriccion, destinatarios);
		
		for(Persona destinatario: destinatarios) {

//			TreeMap<Integer, String> listaservicios = new TreeMap<Integer, String>(destinatario.getServiciosDeNotificaciones());
			
//			System.out.println("Notificado a " + destinatario.getApellido() + " por = " + listaservicios);

//			System.out.println(EnviarMail.enviarMail(infraccion, restriccion.getAgresor(), restriccion.getVictima(), destinatario));
		}
	}

	private void agregarDestinatarios(RestriccionPerimetral restriccion, List<Persona> destinatarios) {

		destinatarios.add(restriccion.getVictima());
		destinatarios.add(restriccion.getAgresor());

	}

}
