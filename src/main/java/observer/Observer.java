package observer;

import java.util.Map;

import pojos.Coordenada;
import pojos.Persona;

public interface Observer {

	//Actulizar cuando el observable notifique
	public void update(Map<Persona, Coordenada> agresoresConUbicacion);

}
