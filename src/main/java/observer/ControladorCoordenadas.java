package observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import json.JsonReaders;
import pojos.Coordenada;
import pojos.Persona;
import pojos.RolEnRestriccion;

public class ControladorCoordenadas implements Subject{

	private List<Observer> observadores;
	
	public UbicacionAgresor listaAgresoresMenosPeligrosos;
	public UbicacionAgresor listaAgresoresMasPeligrosos;
	
	
	public ControladorCoordenadas() {
		this.observadores = new ArrayList<Observer>();
	}
	
	public void enlazarObservador(Observer o) {
		this.observadores.add(o);
	}

	public void desenlazarObservador(Observer o) {
		this.observadores.remove(o);
	}
	
	public void consultarUbicaciones(List<Persona> agresores) {
		
		//DIVIDO LOS AGRESORES EN MAS PELIGROSOS Y MENOS PELIGROSOS
		List<Persona> agresoresMenosPeligrosos = new ArrayList<Persona>();
		List<Persona> agresoresMasPeligrosos = new ArrayList<Persona>();
		
		for(Persona agresor : agresores) {
			if(agresor.getPuntajePeligro().getPuntaje() < 5)
				agresoresMenosPeligrosos.add(agresor);
			else
				agresoresMasPeligrosos.add(agresor);
		}
		
		//CREO AGRESORES CON COORDENADAS VACIAS
		listaAgresoresMenosPeligrosos = new UbicacionAgresor(agresoresMenosPeligrosos);
		listaAgresoresMasPeligrosos = new UbicacionAgresor(agresoresMasPeligrosos);
		
		Timer timerMenosPeligroso = new Timer();
		Timer timerMasPeligroso = new Timer();
		
		//CADA DETERMINADO TIEMPO LE DOY UNA COORDENADA RANDOM A LOS AGRESORES CON ACTUALIZAR COORDENADAS() Y NOTIFICO
		TimerTask tareaMenosPeligrosos = new TimerTask() {
			
			@Override
			public void run() {
				if(agresoresMenosPeligrosos.size()>0) {
					listaAgresoresMenosPeligrosos.actualizarCoordenadas();
					notificar(listaAgresoresMenosPeligrosos.coordenadasAgresor);
				}
			}
		};
		
		TimerTask tareaMasPeligrosos = new TimerTask() {
			
			@Override
			public void run() {
				if(agresoresMasPeligrosos.size()>0) {
					listaAgresoresMasPeligrosos.actualizarCoordenadas();
					notificar(listaAgresoresMasPeligrosos.coordenadasAgresor);
				}
			}
		};

		timerMenosPeligroso.schedule(tareaMenosPeligrosos, 0, 30000);
		timerMasPeligroso.schedule(tareaMasPeligrosos, 100, 10000);
	}

	@Override
	public void notificar(Map<Persona, Coordenada> agresoresConUbicacion) {
		for(int i = 0; i<observadores.size(); i++) {
			this.observadores.get(i).update(agresoresConUbicacion);
		}
	}

/*	
	public static void main(String[] args) {
		
		Persona A = JsonReaders.readRestriccionesPerimetrales().get(0).getAgresor();
		Persona B = JsonReaders.readRestriccionesPerimetrales().get(1).getAgresor();

		A.getPuntajePeligro().actualizarPuntaje(3);
		B.getPuntajePeligro().actualizarPuntaje(8);

		List<Persona> agresores = new ArrayList<Persona>();
		agresores.add(A);
		agresores.add(B);
		
		ControladorDistancias consultor = new ControladorDistancias();
		ControladorCoordenadas ubi = new ControladorCoordenadas();
		ubi.enlazarObservador(consultor);
		ubi.consultarUbicaciones(agresores);
		
	}
*/
	
}
