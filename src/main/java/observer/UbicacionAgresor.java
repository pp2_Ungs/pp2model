package observer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import json.JsonReaders;
import pojos.Coordenada;
import pojos.Persona;

public class UbicacionAgresor {

	Map<Persona, Coordenada> coordenadasAgresor;

	public UbicacionAgresor(List<Persona> agresores) {

		coordenadasAgresor = new HashMap<Persona, Coordenada>();
		Coordenada coordenada = null;

		for (Persona agresor : agresores) {
			coordenadasAgresor.put(agresor, coordenada);
		}
	}

	public Map<Persona, Coordenada> getCoordenadasAgresor() {
		return coordenadasAgresor;
	}

	public void setCoordenadasAgresor(Map<Persona, Coordenada> coordenadasAgresor) {
		this.coordenadasAgresor = coordenadasAgresor;
	}

	// metoodo que consulta al servicio externo la nueva coordenada de los agresores
	public void actualizarCoordenadas() {

		// setear los nuevos valores de coordenadas
		for (Map.Entry<Persona, Coordenada> agresor : coordenadasAgresor.entrySet()) {
			
			Coordenada coordenadaRandom = getCoordenadaRandom();
			agresor.setValue(coordenadaRandom);

		}
	}

	public Coordenada getCoordenadaRandom() {
		return JsonReaders.readCoordenada();
	}
	
}
