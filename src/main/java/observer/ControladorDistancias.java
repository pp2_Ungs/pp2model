package observer;

import java.util.Date;
import java.util.List;
import java.util.Map;

import distanceGateway.DistanceGateway;
import json.JsonReaders;
import negocio.CalculadorDistancias;
import negocio.VoterDistancias;
import pojos.Coordenada;
import pojos.Persona;
import pojos.RestriccionPerimetral;
import stateInfraccion.ControladorInfracciones;

public class ControladorDistancias implements Observer {

	Map<Persona, Coordenada> agresoresConUbicacion;
	CalculadorDistancias calculadorLineal = new CalculadorDistancias();
	ControladorInfracciones controladorInfracciones = new ControladorInfracciones();
	DistanceGateway distanceGateway = new DistanceGateway();
	
	@Override
	public void update(Map<Persona, Coordenada> agresoresConUbicacion) {
		this.agresoresConUbicacion = agresoresConUbicacion;
		hacer();
	}
	
	public void hacer() {
		//RECORRO LOS AGRESORES CON SUS COORDENADAS
		System.out.println("SE ACTUALIZO LA UBICACI�N DE LA PERSONA, SE VERIFICAR� SI EST� INFRINGIENDO LA RESTRICCI�N");
		System.out.print("");
				for (Map.Entry<Persona, Coordenada> agresor : agresoresConUbicacion.entrySet()) {
					
					//RECORRO LAS RESTRICCIONES PARA COMPARAR CON LAS UBICACIONES DE LOS AGRESORES 
					for (RestriccionPerimetral rest : JsonReaders.readRestriccionesPerimetrales()) {

						RestriccionPerimetral restriccion = null;
						
						if (agresor.getKey().getIdPersona() == rest.getAgresor().getIdPersona()) {

							restriccion = rest;
							List<Integer> distancias = distanceGateway.getDistancias(restriccion.getUbicacion(), agresor.getValue());
							
							Integer distanciaVotada = VoterDistancias.elegirDistancia(distancias);
							
							if(distanciaVotada == null)
								distanciaVotada = calculadorLineal.generarDistanciaLineal(restriccion.getUbicacion(), agresor.getValue());
							
							

							System.out.println("idAgresor= " + agresor.getKey().getIdPersona() + ", ubicaci�n actual = " + agresor.getValue());
							System.out.println("La distancia entre la coordenada de restriccion " + restriccion.getUbicacion()
									+ " y la del agresor" + agresor.getValue() + " es de "
									+ distanciaVotada
									+ " metros");
							
							

							if (calculadorLineal.generarDistanciaLineal(restriccion.getUbicacion(), agresor.getValue()) <= restriccion
									.getDistancia())
								System.out.println(restriccion.getAgresor().getApellido() + " esta infringiendo la restriccion de "
										+ restriccion.getDistancia() + " metros");
							else
								System.out.println(restriccion.getAgresor().getApellido() + " no esta infringiendo la restriccion de "
										+ restriccion.getDistancia() + " metros");

							
							if(distanciaVotada<=restriccion.getDistancia())
								controladorInfracciones.verificarInfraccion(restriccion, distanciaVotada, new Date());
							
							System.out.println("----------------------------------------------------");

						}				
					}
					
				}

	}

}
