package observer;

import java.util.Map;

import pojos.Coordenada;
import pojos.Persona;

public interface Subject {

	//Notificar cuando se actualice
	public void notificar(Map<Persona, Coordenada> agresoresConUbicacion);
	
}
