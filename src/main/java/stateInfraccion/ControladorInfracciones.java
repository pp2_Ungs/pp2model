package stateInfraccion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import analizadorInfracciones.AnalizadorInfracciones;
import json.JsonReaders;
import json.JsonUpdaters;
import json.JsonWriters;
import pojos.Infraccion;
import pojos.RestriccionPerimetral;
import pojos.TipoInfraccion;

public class ControladorInfracciones {

	public void verificarInfraccion(RestriccionPerimetral restriccion, int distancia, Date fecha) {
		
		List<Infraccion> infracciones = JsonReaders.readInfraccionesPorRestriccion(restriccion.getId());
		TipoInfraccion tipoInfraccionNueva = AnalizadorInfracciones.obtenerTipoInfraccion(distancia,
				restriccion.getDistancia(), restriccion.getAgresor());
		
		//SI NO HAY INFRACCIONES LA CREO
		if(infracciones.size()==0 )
			JsonWriters.writeInfraccionNueva(crearInfraccion(restriccion, distancia, fecha));

		//CHEQUEO SI NO HAY INFRACCIONES ACTIVA PARA CREAR UN INFRACCION NUEVA 
		else if(!hayInfraccionesActivas(infracciones, restriccion, fecha)) {
			System.out.println("creo una nueva infracci�n");
			JsonWriters.writeInfraccionNueva(crearInfraccion(restriccion, distancia, fecha));
		}
		
		//SO HAY ACTIVAS TENGO QUE CAMBIAR SU ESTADO SEGUN LO QUE HAGA EL AGRESOR
		else {
			for(Infraccion infraccion: infracciones) {
				if(restriccion.getId() == infraccion.getRestriccionPerimetral().getId()) {
					if(diferenciaFechas(infraccion.getFecha(), fecha) < 120){
						
						//CREO UNA INFRACCION CON UN ESTADO NULL QUE LUEGO SE VA A MODIFICAR SEGUN LO QUE HAGA EL AGRESOR
						InfraccionActiva infraccionActiva = new InfraccionActiva(infraccion, null);

						//SI AVANZA Y HACE CAMBIAR EL TIPO DE INFRACCION EDITO TIPO, FECHA, DISTANCIA
						if(infraccion.getTipoInfraccion() != tipoInfraccionNueva &&
								distancia<infraccion.getDistancia()) {
						infraccionActiva.setEstado(new InfraccionAvanza(), infraccion, tipoInfraccionNueva, fecha, distancia);	
						System.out.println("EL AGRESOR AVANZ� SOBRE LA INFRACCI�N GENERADA, SE CAMBIAR� EL TIPO DE infracci�n y se actualizar� la anterior");
						}//SI AVANZA PERO NO CAMBIA TIPO DE INFRACCION EDITO FECHA Y DISTANCIA
						else if(infraccion.getTipoInfraccion() == tipoInfraccionNueva &&
								distancia<infraccion.getDistancia()) {
							infraccionActiva.setEstado(new InfraccionMantiene(), infraccion, tipoInfraccionNueva, fecha, distancia);	
							System.out.println("EL AGRESOR SIGUE INFRINGIENDO LA RESTRICCI�N, PERO LA INFRACCI�N MANTIENE SU TIPO "+tipoInfraccionNueva.toString());
						}
					}
				}
			}
		}
		
	}
	
	private boolean hayInfraccionesActivas(List<Infraccion> infracciones, RestriccionPerimetral restriccion, Date fecha) {
		
		for(Infraccion infraccion: infracciones)
			if(restriccion.getId() == infraccion.getRestriccionPerimetral().getId())
				if(diferenciaFechas(infraccion.getFecha(), fecha) < 120)
					return true;
				
		return false;
	}

	//DIFERENCIA EN SEGUNDOS ENTRE FECHAS
	private int diferenciaFechas(Date fechaGuardada, Date fechaActual) {
		int diferencia = (int) ((fechaActual.getTime() - fechaGuardada.getTime())/1000);
		return diferencia;
	}

	private Infraccion crearInfraccion(RestriccionPerimetral restriccion, int distancia, Date fecha) {
		Infraccion infraccionNueva = new Infraccion(0, fecha, distancia, restriccion);
		System.out.println(".....................");
		System.out.println(infraccionNueva);
		System.out.println(".....................");
		return infraccionNueva;
	}


}
