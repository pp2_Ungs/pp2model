package stateInfraccion;

import java.util.Date;

import pojos.Infraccion;
import pojos.TipoInfraccion;

public class InfraccionActiva {

	Infraccion infraccion;
	EstadoInfraccion estadoInfraccion;
	
	public InfraccionActiva(Infraccion infraccion, EstadoInfraccion estado) {
		this.infraccion = infraccion;
		this.estadoInfraccion = estado;
	}
	
	public void setEstado(EstadoInfraccion estado, Infraccion infraccion, TipoInfraccion tipo, Date fecha, int distancia) {
		this.estadoInfraccion = estado;
		this.estadoInfraccion.editaInfraccion(infraccion, tipo, fecha, distancia);
	}

}
