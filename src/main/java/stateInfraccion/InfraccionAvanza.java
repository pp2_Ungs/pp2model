package stateInfraccion;

import java.util.Date;

import json.JsonUpdaters;
import pojos.Infraccion;
import pojos.TipoInfraccion;

public class InfraccionAvanza implements EstadoInfraccion{

	@Override
	public void editaInfraccion(Infraccion infraccion, TipoInfraccion tipo, Date fecha, int distancia) {
		infraccion.setDistancia(distancia);
		infraccion.setTipoInfraccion(tipo);
		infraccion.setFecha(fecha);
		JsonUpdaters.updateInfraccion(infraccion);
		System.out.println("EDITE INFRACCION. FECHA= "+infraccion.getFecha()+", TIPO= "+infraccion.getTipoInfraccion()+", DISTANCIA= "+infraccion.getDistancia());
	}

}
