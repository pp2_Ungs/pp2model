package stateInfraccion;

import java.util.Date;

import pojos.Infraccion;
import pojos.TipoInfraccion;

public interface EstadoInfraccion {
	
	public void editaInfraccion(Infraccion infraccion, TipoInfraccion tipo, Date fecha, int distancia);
	
}
