package stateInfraccion;

import java.util.Date;

import json.JsonUpdaters;
import pojos.Infraccion;
import pojos.TipoInfraccion;

public class InfraccionMantiene implements EstadoInfraccion{
		
	@Override
	public void editaInfraccion(Infraccion infraccion, TipoInfraccion tipo, Date fecha, int distancia) {
		infraccion.setFecha(fecha);
		infraccion.setDistancia(distancia);
		JsonUpdaters.updateInfraccion(infraccion);
		System.out.println("EDITE INFRACCION. FECHA= "+infraccion.getFecha()+", DISTANCIA= "+infraccion.getDistancia());
	}
	
}
